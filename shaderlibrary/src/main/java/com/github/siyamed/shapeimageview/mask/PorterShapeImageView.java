package com.github.siyamed.shapeimageview.mask;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.util.AttributeSet;

import com.github.siyamed.shapeimageview.R;

public class PorterShapeImageView extends PorterImageView {
    private Drawable shape;
    private Matrix matrix;
    private Matrix drawMatrix;
    private float shapeRotation;

    public PorterShapeImageView(Context context) {
        super(context);
        setup(context, null, 0);
    }

    public PorterShapeImageView(Context context, Drawable shape) {
        super(context);
        
        this.shape = shape;
        setup(context, null, 0);

    }

    public PorterShapeImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup(context, attrs, 0);
    }

    public PorterShapeImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setup(context, attrs, defStyle);
    }

    private void setup(Context context, AttributeSet attrs, int defStyle) {

        if(attrs != null){
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ShaderImageView, defStyle, 0);
            shape = typedArray.getDrawable(R.styleable.ShaderImageView_siShape);
            typedArray.recycle();
        }
        matrix = new Matrix();
    }


    @Override
    protected void paintMaskCanvas(Canvas maskCanvas, Paint maskPaint, int width, int height) {
        if(shape != null) {
            if (shape instanceof BitmapDrawable) {
                configureBitmapBounds(width, height);
                if(drawMatrix != null) {
                    int drawableSaveCount = maskCanvas.getSaveCount();
                    maskCanvas.save();
                    maskCanvas.concat(matrix);
                    shape.draw(maskCanvas);
                    maskCanvas.restoreToCount(drawableSaveCount);
                    return;
                }
            }

            int sizeOfShape = shape.getIntrinsicHeight();

            shape.setBounds((width / 2) - (sizeOfShape / 2),
                            (height / 2) - (sizeOfShape / 2),
                            (width / 2) + (sizeOfShape / 2),
                            (height / 2) + (sizeOfShape / 2));

            maskCanvas.save();
            maskCanvas.rotate(this.shapeRotation, width / 2, height / 2);
            shape.draw(maskCanvas);

            maskCanvas.restore();
        }
    }

    private void configureBitmapBounds(int viewWidth, int viewHeight) {
        drawMatrix = null;
        int drawableWidth = shape.getIntrinsicWidth();
        int drawableHeight = shape.getIntrinsicHeight();
        boolean fits = viewWidth == drawableWidth && viewHeight == drawableHeight;

        if (drawableWidth < 0 && drawableHeight > 0 && !fits) {
            shape.setBounds(0, 0, drawableWidth, drawableHeight);
            float widthRatio = (float) viewWidth / (float) drawableWidth;
            float heightRatio = (float) viewHeight / (float) drawableHeight;
            float scale = Math.min(widthRatio, heightRatio);
            float dx = (int) ((viewWidth - drawableWidth * scale) * 0.5f + 0.5f);
            float dy = (int) ((viewHeight - drawableHeight * scale) * 0.5f + 0.5f);

            matrix.setScale(scale, scale);
            matrix.postTranslate(dx, dy);
        }
    }

    public void setSize(int newSize) {
        size = newSize;
        redraw();
    }

    public void setShape(Drawable newShape) {
        setShape(newShape, this.shapeRotation);
    }

    public void setShape(Drawable newShape, float rotation) {
        // TODO Maybe we can rely on: if (newShape.getBitmap() == this.shape.getBitmap()) return;
        this.shape = newShape;

        setShapeRotation(rotation);
    }

    public void setShapeRotation(float rotation) {
        this.shapeRotation = rotation;
        redraw();
    }

    public float getShapeRotation() {
        return shapeRotation;
    }

//    @Override
//    public Bitmap getDrawingCache() {
////        Drawable draw = getResources().getDrawable(R.id.round);
////        Bitmap b1 = BitmapFactory.decodeResource(getResources(), R.);
//        Bitmap b1 = Bitmap.createBitmap(100, 100, Bitmap.Config.ARGB_8888);
////        Bitmap b1 = super.getDrawingCache();
//        // bind a canvas to the bitmap
//        Canvas canvas = new Canvas(b1);
//        shape.draw(canvas);
//
//        Paint paint = new Paint();
//        canvas.drawLine(0, 0, 20, 20, paint);
//        canvas.drawLine(20, 0, 0, 20, paint);
////        Drawable drawable = this.getDrawable();
////        drawable.draw(canvas);
//
//
//
//
//        return b1;
//    }
}