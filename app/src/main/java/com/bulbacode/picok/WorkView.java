package com.bulbacode.picok;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.bulbacode.picok.activities.EditActivity;
import com.bulbacode.picok.utils.FlurryManager;
import com.bulbacode.picok.utils.ImageCaptureUtils;
import com.bulbacode.picok.utils.PorterSVGImageView;
import com.bulbacode.picok.utils.ShapeMaskRes;
import com.example.android.displayingbitmaps.util.ImageWorker;

/**
 * Created by Hadadi on 1/6/2016.
 *
 * The app's main view
 */
public class WorkView {
    private boolean everChanged;

    public enum ToolFeatureEnum {
        BLUR,
        CHANGE_IMAGE,
        ROTATE,
        MIRROR,
        FLIP,
        COLOR,
        TEXTURE,
        RESIZE,
        ZOOM
    }

    private static WorkView ourInstance = null;

    private PicokImage bgImage;
    private PicokImage upperImage;
    private PicokImage lowerImage;

    // SVG private ShapeMask shapeMask;
    private ShapeMaskRes shapeMask;
    private boolean isInversed;
    private boolean changeUnderlyingImage;
    private int loadCount;
    private Bitmap curOuterBitmap;
    private Bitmap curInnerBitmap;

    private PicokImageSettings curOuterBitmapSettings;
    private PicokImageSettings curInnerBitmapSettings;
    private PicokImageSettings tempOuterSettings;
    private PicokImageSettings tempInnerSettings;

    private PorterSVGImageView innerTextureView;
    private ImageView outerTextureView;
    private ImageView regularOuterTextureView;
    private ImageView inverseOuterTextureView;

    public static WorkView getInstance() {
        if (ourInstance == null)
            ourInstance = new WorkView();

        return ourInstance;
    }

    private WorkView() {
    }

    public void init(String firstBitmapKey, String secondBitmapKey, String thirdBitmapKey) {
        Activity activity = EditActivity.getActivity();

        this.isInversed = false;
        this.changeUnderlyingImage = true;

        // Initializing la bg image
        this.bgImage = new PicokImage((PorterSVGImageView) activity.findViewById(R.id.bg_view),
                                        firstBitmapKey, false);

        this.bgImage.getView().disableMask();
//        this.bgImage.getView().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
//        this.bgImage.getView().redraw();


        // Initializing la lower image with the bg drawable
//        PorterSVGImageView view = (PorterSVGImageView) activity.findViewById(R.id.lower_porter_view);
//        view.setVisibility(View.VISIBLE);
        this.lowerImage = new PicokImage((PorterSVGImageView) activity.findViewById(R.id.lower_porter_view),
                                        secondBitmapKey, false);

//        this.lowerImage.getView().disableMask();
//        this.lowerImage.getView().setVisibility(View.VISIBLE);
//        this.lowerImage.getView().setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_ATOP);
//        this.lowerImage.getView().redraw();

        // Initializing la upper image
        this.upperImage = new PicokImage((PorterSVGImageView) activity.findViewById(R.id.upper_porter_view),
                                        thirdBitmapKey, true);
//        this.upperImage.getView().getDrawable().setColorFilter(Color.YELLOW, PorterDuff.Mode.MULTIPLY);
//        this.upperImage.getView().redraw();

        // Initializing la mask
        // SVG this.shapeMask = new ShapeMask();
        this.shapeMask = new ShapeMaskRes();

        this.lowerImage.getView().setShape(ShapeMaskRes.getShape(), 0);
        this.upperImage.getView().setShape(ShapeMaskRes.getShape(), 0);

        this.innerTextureView = (PorterSVGImageView) activity.findViewById(R.id.inner_texture_view); // InnerTexture;
        this.innerTextureView.setShape(ShapeMaskRes.getShape(), 0);

        this.regularOuterTextureView = (ImageView) activity.findViewById(R.id.regular_outer_texture_view); // OuterRegular;
        this.inverseOuterTextureView = (ImageView) activity.findViewById(R.id.inverse_outer_texture_view);
        this.outerTextureView = regularOuterTextureView;

        showPreview(true);

        //((ImageView) activity.findViewById(R.id.test)).setImageDrawable(ResourcesCompat.getDrawable(activity, R.drawable.shapes_star1));
    }


    public void changeImage(boolean isInner, String newImageID) {
        this.everChanged = true;
        FlurryManager.getInstance().featureEvent(ToolFeatureEnum.CHANGE_IMAGE);

        if (isInner) {
            this.upperImage.loadNewImage(newImageID, false);

            if (isInversed)
                this.bgImage.loadNewImage(newImageID, false);
        }
        else {
            this.lowerImage.loadNewImage(newImageID, false);

            if (!isInversed) {
                this.bgImage.loadNewImage(newImageID, false);
            }
        }

        checkShowPreview();
    }

    private void setNotMultiplied() {
        if (!ShapeMaskRes.getIsMultiplied())
            return;

        switchMultiply();
    }

    public void setMaskRotation(float rotation) {
        // TODO what's the difference between this and rotateMask() ?
        this.upperImage.getView().setShapeRotation(rotation);
        this.lowerImage.getView().setShapeRotation(rotation);
        this.innerTextureView.setShapeRotation(rotation);
    }

    public void rotateMask(float rotation) {
        FlurryManager.getInstance().featureEvent(ToolFeatureEnum.ROTATE);
        this.upperImage.getView().setShapeRotation(rotation);
        this.lowerImage.getView().setShapeRotation(rotation);
        this.innerTextureView.setShapeRotation(rotation);
    }

    private boolean checkShowPreview() {
        // The inner is different from the outer
        if (everChanged) {
            if ((upperImage.getOpacity() != 0 && lowerImage.getOpacity() != 0)) {
                if (ShapeMaskRes.getIsPreview()) {
                    this.shapeMask.setIsPreview(false);
                    showPreview(false);
                }

                return false;
            }
            else {
                if (ShapeMaskRes.getIsPreview()) {
                    this.shapeMask.setIsPreview(true);
                    showPreview(true);
                }

                return true;
            }

        }
        else if ((upperImage.getBlurLevel() != lowerImage.getBlurLevel())
                    ||  (bgImage.getBlurLevel() != lowerImage.getBlurLevel())
                    ||      (upperImage.getView().getZoom() != lowerImage.getView().getZoom())
                    ||  (bgImage.getView().getZoom() != lowerImage.getView().getZoom())
                    ||      (upperImage.getIsMirrored() != lowerImage.getIsMirrored())
                    ||      (bgImage.getIsMirrored() != lowerImage.getIsMirrored())
                    ||          (upperImage.getIsFlipped() != lowerImage.getIsFlipped())
                    ||          (bgImage.getIsFlipped() != lowerImage.getIsFlipped())
                    ||              (bgImage.getColor() != lowerImage.getColor())
                    ||              (upperImage.getColor() != lowerImage.getColor())
                    //||                  TODO SERJ check that oneTexture != otherTexture
                                    ) {
            if (ShapeMaskRes.getIsPreview()) {
                this.shapeMask.setIsPreview(false);
                showPreview(false);
            }

            return false;
        }
        // The inner and outer is the same, the shape won't be visible at all.. hence we show the shape's preview form
        else if (!ShapeMaskRes.getIsPreview()) { // First check that we aren't already showing the preview
            this.shapeMask.setIsPreview(true);
            showPreview(true);
        }

        return true;
    }

    public void showPreview(boolean showPreview) {
        if (!showPreview) {
            PicokImage img = getRightPicokImage(true, false);
            if (img != null)
                img.showTheBitmap();
            /*img = getRightPicokImage(true, true, true);
            if (img != null)
                img.showTheBitmap();
*/
            this.shapeMask.setIsPreview(false);
        }
        else {
            // SVG Drawable drawable = this.shapeMask.setIsPreview(true);
            this.shapeMask.setIsPreview(true);

            // SVG if (drawable != null) {
            // SVG this.upperImage.getView().setShape(drawable);
            // SVG this.lowerImage.getView().setShape(drawable);
            // SVG }

            PicokImage img = getRightPicokImage(true, false);
            if (img != null)
                img.showThePreviewBitmap();
            ///img = getRightPicokImage(true, true, true);
            ///if (img != null)
               /// img.showThePreviewBitmap();
        }
    }

    public void changeMaskAndReset(String shapeName) {
        setNotMultiplied();
        changeMask(shapeName, 0);
        setMaskRotation(0);
    }

    public void changeMask(String shapeName, float rotation) {
        // SVG Drawable newMaskDrawable = this.shapeMask.loadShape(shapeName);
        int newMaskDrawable = ShapeMaskRes.loadShape(shapeName);

        // SVG if (newMaskDrawable != null) {
            this.upperImage.getView().setShape(newMaskDrawable, rotation);
            this.lowerImage.getView().setShape(newMaskDrawable, rotation);
            this.innerTextureView.setShape(newMaskDrawable, rotation);
        // SVG }
    }

    public void changeInverseMask(final boolean doInverse) {
        if (this.isInversed == doInverse)
            return;

        if (doInverse)
            this.tempOuterSettings = new PicokImageSettings(bgImage);
        else
            this.tempOuterSettings = new PicokImageSettings(lowerImage);

        this.tempInnerSettings = new PicokImageSettings(upperImage);

        this.isInversed = doInverse;

        // INVERSINGNGNGNGNGNG
        if (doInverse) {
            extractBitmapAndSettingsFromPicokImage(lowerImage, false);
            extractBitmapAndSettingsFromPicokImage(upperImage, true);
        }
        // UNINVERSINGNGNGNGNGN
        else {
            // TODO OPTION 2 moves the changes done on image1 when it was inner to hide now in lower.
            // Uncommenting this will get image1 inner's changes outside. its better to just move the changes themselves
            // below will make it ASYMMETRIC
            //this.bgImage.swapSrc(this.upperImage);

            extractBitmapAndSettingsFromPicokImage(lowerImage, false);
            extractBitmapAndSettingsFromPicokImage(upperImage, true);
        }
    }

    /**
     * For when we want to save the bitmap and settings of a picok image as it is now before it's changed
     * @param picokImage Which picok image's bitmap and settings do we want to save
     * @param extractForInner In which field we want to save it - this.currInner.. or this.curOuter..? if it's meant to be the new inner - true.
     */
    private void extractBitmapAndSettingsFromPicokImage(PicokImage picokImage, final boolean extractForInner) {
        if (picokImage.getBlurLevel() > 0) {
            picokImage.revert(new ImageWorker.GotBitmapListener() {
                @Override
                public void onGotBitmap(BitmapDrawable resultBitmap) {
                    if (extractForInner) {
                        curInnerBitmap = resultBitmap.getBitmap();
                        curInnerBitmapSettings = PicokImageSettings.defaultSettings(); // We reverted..
                    } else {
                        curOuterBitmap = resultBitmap.getBitmap();
                        curOuterBitmapSettings = PicokImageSettings.defaultSettings(); // We reverted..
                    }

                    onInverseComplete();
                }
            });
        }
        else {
            if (extractForInner) {
                curInnerBitmap = picokImage.getBitmap();
                curInnerBitmapSettings = picokImage.getSettings();
            }
            else {
                curOuterBitmap = picokImage.getBitmap();
                curOuterBitmapSettings = picokImage.getSettings();
            }

            onInverseComplete();
        }
    }

    public void onInverseComplete() {
        if (++this.loadCount < 2)
            return;

        this.loadCount = 0;

        if (this.isInversed) {
            // Lower has no mask now
            lowerImage.getView().disableMask();

            // Switch upper and lower
            // Switching base res
            Object tempResId = lowerImage.getBitmapResID();
            lowerImage.setBitmapResourceID(upperImage.getBitmapResID());
            upperImage.setBitmapResourceID(tempResId);

            // switching current bitmaps with their settings, but changing their settings to the old saved settings
            lowerImage.reapplySavedSettings(curInnerBitmap, curInnerBitmapSettings, tempOuterSettings);
            upperImage.reapplySavedSettings(curOuterBitmap, curOuterBitmapSettings, tempInnerSettings);

            // Lower gets the opacity
            lowerImage.setOpacity(upperImage.getOpacity());
            upperImage.setOpacity(1);

            // Switching outer texture view
            Drawable tempDrawable = outerTextureView.getDrawable();
            outerTextureView.setImageDrawable(null);
            outerTextureView = inverseOuterTextureView;
            outerTextureView.setImageDrawable(tempDrawable);

        }
        else {
            // Lower has a mask now
            this.lowerImage.getView().enableMask();

            // Switch upper and lower
            // Switching base res
            Object tempResId = lowerImage.getBitmapResID();
            lowerImage.setBitmapResourceID(upperImage.getBitmapResID());
            upperImage.setBitmapResourceID(tempResId);

            // switching current bitmaps with their settings, but changing their settings to the old saved settings
            bgImage.reapplySavedSettings(tempOuterSettings);
            upperImage.reapplySavedSettings(curOuterBitmap, curOuterBitmapSettings, tempInnerSettings);
            lowerImage.reapplySavedSettings(curInnerBitmap, curInnerBitmapSettings, tempInnerSettings);

            // Upper gets the opacity
            upperImage.setOpacity(lowerImage.getOpacity());
            lowerImage.setOpacity(1);

            // Switching outer texture view
            Drawable tempDrawable = outerTextureView.getDrawable();
            outerTextureView.setImageDrawable(null);
            outerTextureView = regularOuterTextureView;
            outerTextureView.setImageDrawable(tempDrawable);
        }

        // For when if after all switching we got a bit confused on weather we show the preview image or not
        // (like if we did set the preview image to be shown, but then applied a switched image and..)
        showPreview(ShapeMaskRes.getIsPreview());
    }

    public void mirror(boolean isInner) {
        //noinspection ConstantConditions
        FlurryManager.getInstance().featureEvent(ToolFeatureEnum.MIRROR);

        getRightPicokImage(isInner, false).reflect(true, false);

        if (this.changeUnderlyingImage) {
            PicokImage picokImage = getRightPicokImage(isInner, true);
            if (picokImage != null)
                picokImage.reflect(true, false);
        }

        checkShowPreview();
    }

    public void flip(boolean isInner) {
        //noinspection ConstantConditions
        FlurryManager.getInstance().featureEvent(ToolFeatureEnum.FLIP);
        getRightPicokImage(isInner, false).reflect(false, true);

        if (this.changeUnderlyingImage) {
            PicokImage picokImage = getRightPicokImage(isInner, true);
            if (picokImage != null)
                picokImage.reflect(false, true);
        }

        checkShowPreview();
    }

    public void setZoom(int zoom, boolean isInner) {
        //noinspection ConstantConditions
        FlurryManager.getInstance().featureEvent(ToolFeatureEnum.ZOOM);
        getRightPicokImage(isInner, false).setZoom(zoom);

        if (this.changeUnderlyingImage) {
            PicokImage picokImage = getRightPicokImage(isInner, true);
            if (picokImage != null)
                picokImage.setZoom(zoom);
        }

        checkShowPreview();
    }

    public int getZoom(boolean isInner) {
        //noinspection ConstantConditions
        return getRightPicokImage(isInner, false).getZoom();
    }

    public boolean getIsMirrored(boolean isInner) {
        //noinspection ConstantConditions
        return getRightPicokImage(isInner, false).getIsMirrored();
    }

    public boolean getIsFlipped(boolean isInner) {
        //noinspection ConstantConditions
        return getRightPicokImage(isInner, false).getIsFlipped();
    }

    public void setMaskSize(int newSize) {
        FlurryManager.getInstance().featureEvent(ToolFeatureEnum.RESIZE);

        int oldSize = getMaskSize();

        if (oldSize == newSize)
            return;

        this.upperImage.getView().setSize(newSize);
        this.lowerImage.getView().setSize(newSize);
        this.innerTextureView.setSize(newSize);

        // TODO Look into this
        // We can't do matrix operations on the biggest size multiplied form. so disable the multiply
        if (newSize == 3 && ShapeMaskRes.getIsMultiplied()) {
            int drawable = ShapeMaskRes.getNotMultipliedVersion();

            this.upperImage.getView().setShape(drawable);
            this.lowerImage.getView().setShape(drawable);
            this.bgImage.getView().setShape(drawable);
            this.innerTextureView.setShape(drawable);
        }
        else if (oldSize == 3 && ShapeMaskRes.getIsMultiplied()) {
            int drawable = ShapeMaskRes.getMultipliedVersion();

            this.upperImage.getView().setShape(drawable);
            this.lowerImage.getView().setShape(drawable);
            this.bgImage.getView().setShape(drawable);
            this.innerTextureView.setShape(drawable);
        }
    }

    public void setBlur(int newblurLevel, boolean isInner) {
        //noinspection ConstantConditions
        FlurryManager.getInstance().featureEvent(ToolFeatureEnum.BLUR);
        getRightPicokImage(isInner, false).blur(newblurLevel);

        if (this.changeUnderlyingImage) {
            PicokImage picokImage = getRightPicokImage(isInner, true);
            if (picokImage != null)
                picokImage.blur(newblurLevel);
        }

        checkShowPreview();
    }

    public int getBlur(boolean isInner) {
        if (isInner)
            return upperImage.getBlurLevel();
        else
            return bgImage.getBlurLevel();
    }

    /**
     * Gets the right PicokImage (bg, lower or upper)
     * @param isInner Get the inner or outer
     * @param getTheOneBeneathOpacity When there are two PicokImages to get and this is set to true, get the one beneath the opacity (when opacity isn't full)
     * @return The picokImage
     */
    private PicokImage getRightPicokImage(boolean isInner, boolean getTheOneBeneathOpacity) {
        return getRightPicokImage(isInner, getTheOneBeneathOpacity, false);
    }

    /**
     * Gets the right PicokImage (bg, lower or upper)
     * @param isInner Get the inner or outer
     * @param getTheOneBeneathOpacity When there are two PicokImages to get and this is set to true, get the one beneath the opacity (when opacity isn't full)
     * @param forceGetBeneathOpacity Force getting the PicokImage beneath opacity even when opacity is full
     * @return The picokImage
     */
    private PicokImage getRightPicokImage(boolean isInner, boolean getTheOneBeneathOpacity, boolean forceGetBeneathOpacity) {
        if (isInner) {
            if (!isInversed) {
                if (!getTheOneBeneathOpacity) {
                    return this.upperImage;
                }
                // Return the underlying only if opacity isn't full,
                // so we will change the underlying only when its visible.
                // or when we force it.
                else if (forceGetBeneathOpacity || this.upperImage.getView().getAlpha() < 1) {
                    return this.lowerImage;
                }
                else {
                    return null;
                }
            }
            // Its inversed's inner
            else {
                if (!getTheOneBeneathOpacity) {
                    return this.upperImage;
                }
                else {
                    return null;
                }
            }
        }
        // Its outer
        else {
            if (!isInversed) {
                if (!getTheOneBeneathOpacity) {
                    return this.bgImage;
                }
                else {
                    return null;
                }
            }
            // Its inversed's Outer
            else {
                if (!getTheOneBeneathOpacity) {
                    return this.lowerImage;
                }
                // Return the underlying only if opacity isn't full,
                // so we will change the underlying only when its visible
                else if (this.lowerImage.getView().getAlpha() < 1) {
                    return this.bgImage;
                }
                else {
                    return null;
                }
            }
        }
    }

    public void switchMultiply() {
        // SVG Drawable drawable = this.shapeMask.switchMultiplied();
        int drawable = ShapeMaskRes.switchMultiplied();

        // SVG if (drawable != null) {
            this.upperImage.getView().setShape(drawable);
            this.lowerImage.getView().setShape(drawable);
            this.bgImage.getView().setShape(drawable);
            this.innerTextureView.setShape(drawable);
        // SVG }
    }

    public boolean getIsMultiplied() {
        // SVG return ShapeMask.getIsMultiplied();
        return ShapeMaskRes.getIsMultiplied();
    }

    public boolean getIsInversed() {
        return this.isInversed;
    }

    public void setOpacity(float opacity) {
        if (!this.isInversed)
            this.upperImage.setOpacity(opacity);
        else
            this.lowerImage.setOpacity(opacity);

        checkShowPreview();
    }

    public float getOpacity() {
        if (!this.isInversed)
            return this.upperImage.getOpacity();
        else
            return this.lowerImage.getOpacity();
    }

    public PicokImage getLower() {
        return this.lowerImage;
    }



    public void setColorTextureOpacity(float opacity, boolean isTextureInner) {
        if (isTextureInner) {
            this.innerTextureView.setAlpha(opacity);
        }
        else {
            this.outerTextureView.setAlpha(opacity);
        }
        // TODO remove the colorAlpha
//        this.colorAlpha = opacity;
    }

    public void setColor(int newColor, boolean isInner, float alphaFactor) {
        //noinspection ConstantConditions
        getRightPicokImage(isInner, false).setColor(newColor, alphaFactor);

        if (this.changeUnderlyingImage) {
            PicokImage picokImage = getRightPicokImage(isInner, true);
            if (picokImage != null)
                picokImage.setColor(newColor, alphaFactor);
        }

//        // When setting color for inner / outer we reset the color of the other (outer / inner)
//        //noinspection ConstantConditions
//        getRightPicokImage(!isInner, false).setColor(Color.TRANSPARENT);
//
//        if (this.changeUnderlyingImage) {
//            PicokImage picokImage = getRightPicokImage(!isInner, true);
//            if (picokImage != null)
//                picokImage.setColor(Color.TRANSPARENT);
//        }

        checkShowPreview();
    }

    public void setTexture(Drawable drawable, boolean isInner) {
        FlurryManager.getInstance().featureEvent(ToolFeatureEnum.TEXTURE);
        if (isInner) {
            Bitmap drawableBitmap = ImageCaptureUtils.drawableToBitmap(drawable);
            innerTextureView.setImageBitmap(drawableBitmap);
//            outerTextureView.setImageDrawable(null);
        }
        else {
            outerTextureView.setImageDrawable(drawable);
//            innerTextureView.setImageDrawable(null);
        }
    }

    public int getMaskSize() {
        return this.upperImage.getView().getSize();
    }
}