package com.bulbacode.picok;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.Element;
import android.support.v8.renderscript.RenderScript;
import android.support.v8.renderscript.ScriptIntrinsicBlur;

import com.bulbacode.picok.activities.EditActivity;
import com.bulbacode.picok.utils.ImageCaptureUtils;

/**
 * Created by Hadadi on 1/17/2016.
 *
 * Makes all the bitmap operations
 */
public class BitmapManipulator {
    public static Bitmap blur(Bitmap bitmap, float blurLevel) {
        final RenderScript rs = RenderScript.create(EditActivity.getActivity());
        final Allocation input = Allocation.createFromBitmap(rs, bitmap, Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_SCRIPT);
        final Allocation output = Allocation.createTyped(rs, input.getType());

        final ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        script.setRadius(blurLevel);
        script.setInput(input);
        script.forEach(output);

        output.copyTo(bitmap);
        rs.destroy();
        return bitmap;
    }

    public static Bitmap reflect(Bitmap src, boolean isHorizontal, boolean isVertical) {
        Matrix matrix = new Matrix();
        int width = src.getWidth();
        int height = src.getHeight();

        matrix.preScale(isVertical ? 1.0f : -1.0f,
                isHorizontal ? 1.0f : -1.0f);

        return Bitmap.createBitmap(src, 0, 0, width, height, matrix, true);
    }

    public static Bitmap changeColor(Bitmap bitmapToColor, int color, float alphaFactor) {
        // color and canvas settings
        Canvas canvas = new Canvas(bitmapToColor);
        Paint paint = new Paint();

        int colorWithAlpha = ImageCaptureUtils.adjustAlpha(color, alphaFactor);
        ColorFilter filter = new PorterDuffColorFilter(colorWithAlpha, PorterDuff.Mode.SRC_ATOP);
        paint.setColorFilter(filter);
        canvas.drawBitmap(bitmapToColor, 0, 0, paint);
        return bitmapToColor;
    }
}
