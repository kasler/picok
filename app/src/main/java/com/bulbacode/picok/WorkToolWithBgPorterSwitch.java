package com.bulbacode.picok;

import android.view.View;
import android.widget.ImageButton;

import com.bulbacode.picok.worktools.WorkTool;

/**
 * Created by Hadadi on 1/5/2016.
 *
 * For all tools that use bg-porter switch button
 */
public class WorkToolWithBgPorterSwitch extends WorkTool {

    private final ImageButton outerButton;
    private final ImageButton innerButton;
    private final ImageButton outerStatus;
    private final ImageButton innerStatus;
    protected boolean isInner;
    protected boolean isOuter;
    public boolean activateInnerButtonOnDefault;

    private boolean isInnerAltered;
    private boolean isOuterAltered;

    public WorkToolWithBgPorterSwitch(int bgPorterSwitchLayoutRes, int toolLayout,
                                        int inactiveButtonResource, int activeButtonResource,
                                            int toolButtonResource) {
        super(toolLayout, inactiveButtonResource,
                activeButtonResource, toolButtonResource);

        View bgPorterSwitchView = this.toolView.findViewById(bgPorterSwitchLayoutRes);

        this.outerButton = (ImageButton) bgPorterSwitchView.findViewById(R.id.bg_switch_button);
        this.innerButton = (ImageButton) bgPorterSwitchView.findViewById(R.id.porter_switch_button);

        this.outerStatus = (ImageButton) bgPorterSwitchView.findViewById(R.id.out_status);
        this.innerStatus = (ImageButton) bgPorterSwitchView.findViewById(R.id.in_status);

        // Hooking the bg and porter buttons
        this.outerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchToOuter();
            }
        });
        this.innerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchToInner();
            }
        });

        this.activateInnerButtonOnDefault = true;

        switchToNone();
    }

    protected void switchToInner() {
        if (isInner)
            return;

        this.isInner = true;
        this.isOuter = false;

        this.innerButton.setImageResource(R.drawable.in_sel);
        this.outerButton.setImageResource(R.drawable.out);

        this.innerStatus.setVisibility(View.VISIBLE);
        this.innerStatus.setImageResource(R.drawable.sel_line);

        if (this.isOuterAltered) {
            this.outerStatus.setVisibility(View.VISIBLE);
            this.outerStatus.setImageResource(R.drawable.dot);
        }
        else {
            this.outerStatus.setVisibility(View.INVISIBLE);
        }

        onSwitchToInner();
    }

    protected void switchToOuter() {
        if (isOuter)
            return;

        this.isInner = false;
        this.isOuter = true;

        this.outerButton.setImageResource(R.drawable.out_sel);
        this.innerButton.setImageResource(R.drawable.in);

        this.outerStatus.setVisibility(View.VISIBLE);
        this.outerStatus.setImageResource(R.drawable.sel_line);

        if (this.isInnerAltered) {
            this.innerStatus.setVisibility(View.VISIBLE);
            this.innerStatus.setImageResource(R.drawable.dot);
        }
        else {
            this.innerStatus.setVisibility(View.INVISIBLE);
        }

        onSwitchToOuter();
    }

    protected void onSwitchToOuter() {}

    protected void onSwitchToInner() {}

    protected void onActivate() {
        super.onActivate();

        isInner = false;
        isOuter = false;

        if (this.activateInnerButtonOnDefault) {
            // Initializing state
            switchToInner();
        }
        else {
            switchToNone();
        }
    }

    // TODO if there's no way both buttons are inactive anywhere, remove this
    public void switchToNone() {
        this.isOuter = false;
        this.isInner = false;

        this.outerButton.setImageResource(R.drawable.out);
        this.innerButton.setImageResource(R.drawable.in);

        if (this.isInnerAltered) {
            this.innerStatus.setVisibility(View.VISIBLE);
            this.innerStatus.setImageResource(R.drawable.dot);
        }
        else {
            this.innerStatus.setVisibility(View.INVISIBLE);
        }

        if (this.isOuterAltered) {
            this.outerStatus.setVisibility(View.VISIBLE);
            this.outerStatus.setImageResource(R.drawable.dot);
        }
        else {
            this.outerStatus.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * Call this when you don't want the default "inner" button be active each time going to this tool
     */
    protected void disableActivateInnerButtonOnDefault() {
        this.activateInnerButtonOnDefault = false;
    }

    public void setIsInnerAltered(boolean isInnerAltered) {
        this.isInnerAltered = isInnerAltered;

        if (this.isInnerAltered) {
            this.innerStatus.setVisibility(View.VISIBLE);
            this.innerStatus.setImageResource(R.drawable.dot);
        }
        else {
            this.innerStatus.setVisibility(View.INVISIBLE);
        }
    }

    public void setIsOuterAltered(boolean isOuterAltered) {
        this.isOuterAltered = isOuterAltered;

        if (this.isOuterAltered) {
            this.outerStatus.setVisibility(View.VISIBLE);
            this.outerStatus.setImageResource(R.drawable.dot);
        }
        else {
            this.outerStatus.setVisibility(View.INVISIBLE);
        }
    }
}
