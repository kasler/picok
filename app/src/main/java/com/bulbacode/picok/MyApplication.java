package com.bulbacode.picok;

import android.app.Application;
import android.util.Log;

import com.flurry.android.FlurryAgent;

import pl.droidsonroids.gif.LibraryLoader;

/**
 * Created by sergey on 1/5/16.
 */
public class MyApplication extends Application {

    private static final String TAG = MyApplication.class.getSimpleName();
    private static final String MY_FLURRY_APIKEY = "K23YBQB9YVB43J56P8VP";
    public boolean writePermission;


    public void onCreate()
    {
        super.onCreate();

        LibraryLoader.initialize(this);

        Thread.setDefaultUncaughtExceptionHandler (new Thread.UncaughtExceptionHandler()
        {
            @Override
            public void uncaughtException (Thread thread, Throwable e)
            {
                handleUncaughtException (thread, e);
            }
        });

        // configure Flurry
        FlurryAgent.setLogEnabled(true);

        // init Flurry
        FlurryAgent.init(this, MY_FLURRY_APIKEY);
    }

    private void handleUncaughtException (Thread thread, Throwable e) {
        Log.e(TAG, "exception", e);
    }
}
