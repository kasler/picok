package com.bulbacode.picok;

import android.annotation.TargetApi;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;

import com.bulbacode.picok.activities.EditActivity;
import com.bulbacode.picok.utils.PorterSVGImageView;
import com.example.android.displayingbitmaps.util.ImageWorker;

/**
 * Created by Hadadi on 1/6/2016.
 *
 * An image that can have Picok operations on
 */
public class PicokImage {
    private static final int DEFAULT_BLUR_LEVEL = 0;
    private static final float DEFAULT_ALPHA = 0.5f;

    private final Resources resources;

    private BitmapFactory.Options options;
    private PorterSVGImageView view;
    private boolean isMirrored;
    private boolean isFlipped;
    private int blurLevel;

    private Bitmap bitmap;
    private Object bitmapResourceID;

//    private int bitmapResourceID;
    private int color;
    private float alphaFactor;


    public PicokImage(PorterSVGImageView view) {
        this.isMirrored = false;
        this.isFlipped = false;
        this.blurLevel = DEFAULT_BLUR_LEVEL;

        this.view = view;
        this.alphaFactor = DEFAULT_ALPHA;

        this.resources = EditActivity.getActivity().getResources();
    }

    public PicokImage(PorterSVGImageView view, int resID, boolean loadAsPreview) {
        this(view);
        // set the size to option, the images we will load by using this option
        options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inMutable = true;
        BitmapFactory.decodeResource(resources, resID, options);

        // we will create empty bitmap by using the option
        bitmap = Bitmap.createBitmap(options.outWidth, options.outHeight, Bitmap.Config.ARGB_8888);

        // set the option to allocate memory for the bitmap
        options.inJustDecodeBounds = false;
        options.inSampleSize = 1;
        //options.inBitmap = bitmap;
        options.inMutable = true;

        loadNewImage(resID, loadAsPreview);
    }

    public PicokImage(PorterSVGImageView viewById, String id, boolean loadAsPreview) {
        this(viewById);
        loadNewImage(id, loadAsPreview);
    }


    public void loadNewImage(Object  id, final boolean previewLoad) {
        EditActivity.picokImageWorker.loadImage(id, this.view, true, true, new ImageWorker.GotBitmapListener() {
            @Override
            public void onGotBitmap(BitmapDrawable newBitmap) {
                bitmap = newBitmap.getBitmap();

                if (!previewLoad)
                    reapplyOperations();
                else
                    showThePreviewBitmap();
            }
        });

        setBitmapResourceID(id);
    }


    public void setBitmapResourceID(Object resID) {
        this.bitmapResourceID = resID;
    }

    /**
     * Blur
     *
     * @param newBlurLevel new blur level
     * @return true when setting a new bitmap to the view
     */
    public boolean blur(final int newBlurLevel) {
//        if (this == WorkView.getInstance().getLower())
//            TODO WTF is this 56 ?
//            bitmapResourceID = 56;

        if (this.blurLevel == newBlurLevel)
            return false;

        if (blurLevel > 0) {
            /* TODO MULTITHREADED EditActivity.picokImageWorker.loadImage(this.bitmapResourceID, this.view, true,
                    true, new ImageWorker.GotBitmapListener() {
                        @Override
                        public void onGotBitmap(BitmapDrawable newBitmap) {
                            bitmap = newBitmap.getBitmap();
*/
                            Bitmap newBitmap = EditActivity.getImageCache().getBitmapFromDiskCache(String.valueOf(this.bitmapResourceID));
                            bitmap = newBitmap;
            /**/
                            blurLevel = 0;
                            this.reflect(this.isMirrored, this.isFlipped, false);
                            blurLevel = newBlurLevel;

                            if (newBlurLevel == 0) {
                                view.setImageBitmap(bitmap);
                                return true;
                            }

                            //bitmap = BitmapManipulator.blur(newBitmap.getBitmap(), (newBlurLevel * 6) + 1);
                            bitmap = BitmapManipulator.blur(newBitmap, (newBlurLevel * 6) + 1);

                            view.setImageBitmap(bitmap);
                /*        }
                    });*/
        }
        else {
            bitmap = BitmapManipulator.blur(bitmap, (newBlurLevel * 6) + 1);

            view.setImageBitmap(bitmap);
        }

        this.blurLevel = newBlurLevel;

        return true;
    }

    /**
     * Returning to base, then applying all other operations
     */
    private void reapplyOperations() {

        this.reflect(this.isMirrored, this.isFlipped);

        if (this.color > 0 && this.alphaFactor > 0.0f) {
            setColor(this.color, this.alphaFactor);
        }

        if (this.blurLevel > 0) {
            int targetBlur = this.blurLevel;
            this.blurLevel = 0;
            this.blur(targetBlur);
        }
        else
            this.view.setImageBitmap(bitmap); // When not blurring, we can update now..
                                                // otherwise blur will update when completed
    }

    public void setZoom(int newZoom) {
        setZoom(newZoom, false);
    }

    public void setZoom(int newZoom, boolean justTrackZoomLevel) {
        this.view.setZoom(newZoom, justTrackZoomLevel);
    }

    public void reflect(boolean mirror, boolean flip) {
        reflect(mirror, flip, true);
    }
    public void reflect(boolean mirror, boolean flip, boolean redraw) {
        if (!mirror && !flip)
            return;

        // Reflecting
        this.bitmap = BitmapManipulator.reflect(this.bitmap, mirror, flip);

        if (redraw)
            this.view.setImageBitmap(this.bitmap);

        if (mirror)
            this.isMirrored = !this.isMirrored;

        if (flip)
            this.isFlipped = !this.isFlipped;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void setOpacity(float newOpacity) {
        this.view.setBorderAlpha(newOpacity);
        this.view.setAlpha(newOpacity);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public float getOpacity() {
        return this.view.getAlpha();
    }

    public int getZoom() {
        return this.view.getZoom();
    }

    public boolean getIsMirrored() {
        return this.isMirrored;
    }

    public boolean getIsFlipped() {
        return this.isFlipped;
    }

    public int getBlurLevel() {
        return this.blurLevel;
    }

    public PorterSVGImageView getView() {
        return view;
    }



    public Object getBitmapResID() {
        return this.bitmapResourceID;
    }

    public void reapplySavedSettings(PicokImageSettings oldSettingsToApply) {
        reapplySavedSettings(bitmap, new PicokImageSettings(this), oldSettingsToApply);
    }

    public void reapplySavedSettings(Bitmap newBitmap, PicokImageSettings theBitmapSettings,
                                     PicokImageSettings oldSettingsToApply) {
        this.bitmap = newBitmap;

        // Color doesn't affect the bitmap - just the view, so we don't care about the color of newBitmap cuz there's none
        this.setColor(oldSettingsToApply.color, alphaFactor);

        // If we only need to change the bitmap, set it.. otherwise the settings changes will do this
        if (theBitmapSettings.equals(oldSettingsToApply)) {
            this.view.setImageBitmap(bitmap);
            this.isFlipped = theBitmapSettings.isFlipped;
            this.isMirrored = theBitmapSettings.isMirrored;
            this.blurLevel = theBitmapSettings.blurLevel;
            this.setZoom(theBitmapSettings.zoomLevel, true);
            this.color = theBitmapSettings.color;
            return;
        }

        boolean doFlip = false;
        boolean doMirror = false;

        if (oldSettingsToApply.isFlipped != theBitmapSettings.isFlipped)
            doFlip = true;

        if (oldSettingsToApply.isMirrored != theBitmapSettings.isMirrored)
            doMirror = true;

        reflect(doMirror, doFlip, false);

        this.isMirrored = oldSettingsToApply.isMirrored;
        this.isFlipped = oldSettingsToApply.isFlipped;

        this.setZoom(theBitmapSettings.zoomLevel, true); // Only applying zoom = zoomLevel, not actually zooming

        setZoom(oldSettingsToApply.zoomLevel);

        this.blurLevel = theBitmapSettings.blurLevel;

        // If blur didn't set the bitmap, we will
        if (!blur(oldSettingsToApply.blurLevel))
            this.view.setImageBitmap(bitmap);

        if (this.color > 0) {
            // TODO what to do with the alpha ?
            this.setColor(this.color, this.alphaFactor);
        }
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void revert(final ImageWorker.GotBitmapListener listener) {
        /* TODO MULTITHREADED EditActivity.picokImageWorker.loadImage(bitmapResourceID, view,
                                                true, true, new ImageWorker.GotBitmapListener() {
                    @Override
                    public void onGotBitmap(BitmapDrawable newBitmap) {*/
                        BitmapDrawable newBitmap = new BitmapDrawable(EditActivity.getActivity().getResources(),
                                EditActivity.getImageCache().getBitmapFromDiskCache(String.valueOf(bitmapResourceID)));
                        listener.onGotBitmap(newBitmap);
              /*      }
                });*/
    }

    public PicokImageSettings getSettings() {
        return new PicokImageSettings(this);
    }

    public void showTheBitmap() {
        this.getView().setImageBitmap(bitmap);
        this.getView().setIsPreview(false);
    }

    public void showThePreviewBitmap() {
        this.getView().setImageResource(R.drawable.shape_selected);
        this.getView().setIsPreview(true);
    }

    public void setColor(int color, float alphaFactor) {
        if (this.color == color && this.alphaFactor == alphaFactor)
            return;

        this.color = color;
        this.alphaFactor = alphaFactor;

        Bitmap newBitmap = EditActivity.getImageCache().getBitmapFromDiskCache(String.valueOf(this.bitmapResourceID));

        BitmapManipulator.changeColor(newBitmap, color, alphaFactor);
        bitmap = newBitmap;
        getView().setImageBitmap(bitmap);
    }

    public int getColor() {
        return color;
    }
}