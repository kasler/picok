package com.bulbacode.picok;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;

import com.bulbacode.picok.activities.EditActivity;

import pl.droidsonroids.gif.GifDrawable;

/**
 * Created by Hadadi on 1/20/2016.
 *
 * The shape object when it's being observed in the shape tool
 */
public class ShapeChoice {
    private static ShapeChoice selectedItem = null;

    private final int res;
    private final String shapeName;
    private final boolean isRotatable;
    private boolean isSelected;
    private boolean isFirstUpdate;

    RotateAnimation rotateAnim;
    private int rotation;
    private boolean justSelected;
    private boolean justDeselected;

    public ShapeChoice(String shapeName, boolean isRotatable, Resources resources) {
        this.isRotatable = isRotatable;
        this.isSelected = false;
        this.isFirstUpdate = true;

        this.res = resources.getIdentifier(shapeName, "drawable",
                                                    EditActivity.getActivity().getPackageName());

        this.shapeName = shapeName + "_" + shapeName;
    }

    public String getName() {
        return shapeName;
    }

    public void updateView(ShapeListAdapter.ViewHolder shape) {
        shape.frame.setLayoutParams(new FrameLayout.LayoutParams(EditActivity.screenWidth / 4,
                                                                    ViewGroup.LayoutParams.MATCH_PARENT));

        //EditActivity.picokImageWorker.loadImage(this.active_res, shape.shapeButton);
        shape.shapeButton.setImageResource(this.res);

        if (this.isSelected) {
            shape.shapeButton.setColorFilter(ContextCompat.getColor(EditActivity.getActivity().getApplicationContext(), R.color.picok_pink), PorterDuff.Mode.SRC_IN);
            shape.activeView.setVisibility(View.VISIBLE);
        }
        else {
            shape.shapeButton.clearColorFilter();
            shape.activeView.setVisibility(View.INVISIBLE);
        }

        // TODO If we DO want the rotation to reset when deselecting: resetRotation(); instead
        // Rotate to current rotation (rotation resets when changing the view's source)
        if (this.rotation != 0)
            applyRotation(shape.shapeButton);

        if (isRotatable) {
            if (this.isSelected) {
                if (justSelected) {
                    justSelected = false;

                    // Apply the in animation
                    shape.rotateButton.setImageResource(R.drawable.rotate_in);
                    //((GifDrawable) shape.rotateButton.getDrawable()).start();
                }
            }
            else {
                if (justDeselected) {
                    justDeselected = false;

                    // Apply the out animation
                    shape.rotateButton.setImageResource(R.drawable.rotate_out);
                    //((GifDrawable) shape.rotateButton.getDrawable()).start();
                }
                else if (isFirstUpdate) {
                    shape.rotateButton.setImageResource(R.drawable.rotate_in);
                    ((GifDrawable) shape.rotateButton.getDrawable()).stop();
                }
            }
        }
        else {
            shape.rotateButton.setVisibility(View.INVISIBLE);
        }
    }

    public float rotate(View shape) {
        // If this shape isn't even selected, we shouldn't rotate it
        if (!this.isSelected)
            return -1;

        if (rotateAnim != null && rotateAnim.hasStarted() && !rotateAnim.hasEnded()) {
            return -1;
        }

        this.rotation += 45;
        if (this.rotation == 360)
            this.rotation = 0;

        applyRotation(shape);

        return this.rotation;
    }

    public void applyRotation(View shape) {
        if (rotateAnim != null && rotateAnim.hasStarted() && !rotateAnim.hasEnded()) {
            rotateAnim.cancel();
        }

        rotateAnim = new RotateAnimation(this.rotation, this.rotation,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        rotateAnim.setDuration(0);
        rotateAnim.setFillAfter(true);

        shape.startAnimation(rotateAnim);
    }

    private void resetRotation() {
        this.rotation = 0;
    }

    public void select() {
        if (this.isSelected)
            return;

        this.isSelected = true;

        this.justSelected = true;

        ShapeChoice lastSelectedItem = selectedItem;

        selectedItem = this;

        if (lastSelectedItem != null)
            lastSelectedItem.deselect();

        // TODO If we reset rotations.. WorkView.getInstance().changeMaskAndReset(shapeChoice.getName());
        WorkView.getInstance().changeMask(this.getName(), this.rotation);
    }

    private void deselect() {
        this.isSelected = false;
        justDeselected = true;
        this.rotation = 0;
    }

    public boolean getIsSelected() {
        return isSelected;
    }

    public static void reset(ShapeChoice[] shapes) {
        if (shapes != null) {
            for (ShapeChoice shape : shapes)
                shape.reset();
        }

        selectedItem = null;
    }

    private void reset() {
        deselect();
        resetRotation();
    }
}
