package com.bulbacode.picok;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.util.LruCache;

import com.bulbacode.picok.activities.EditActivity;

import java.lang.ref.SoftReference;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Hadadi on 1/13/2016.
 *
 * Stores a cache of bitmaps using Android's LruCache
 */
public class BitmapLibrary extends LruCache<String, Bitmap> {
    private static BitmapLibrary instance = null;

    private Set<SoftReference<Bitmap>> reusableBitmaps;

    public static BitmapLibrary getInstance() {
        if (instance == null) {
            Context context = EditActivity.getActivity();

            final int memClass = ((ActivityManager) context.getSystemService(
                    Context.ACTIVITY_SERVICE)).getMemoryClass();

            // Use 1/8th of the available memory for this memory cache.
            final int cacheSize = 1024 * 1024 * memClass / 8;

            instance = new BitmapLibrary(cacheSize);
        }

        return instance;
    }

    private BitmapLibrary(int cacheSize) {
        super(cacheSize);

        // If running on Honeycomb or newer, create a
        // synchronized HashSet of references to reusable bitmaps.
        // if (Utils.hasHoneycomb()) {
            reusableBitmaps =
                    Collections.synchronizedSet(new HashSet<SoftReference<Bitmap>>());
        //}
    }

    protected int sizeOf(String key, Bitmap bitmap) {
        // The cache size will be measured in bytes rather than number
        // of items.
        return bitmap.getByteCount();
    }

    // Notify the removed entry that is no longer being cached.
    protected void entryRemoved(boolean evicted, String key,
                                Bitmap oldValue, Bitmap newValue) {
        // Add the bitmap to a SoftReference set for possible use with inBitmap later.
        reusableBitmaps.add(new SoftReference<Bitmap>(oldValue));
    }
}
