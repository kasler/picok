package com.bulbacode.picok.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.graphics.Point;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bulbacode.picok.R;
import com.bulbacode.picok.utils.InterceptingFrameLayout;

/**
 * Created by Hadadi on 4/30/2016.
 */
public final class TutorialManager {
    private static View tutorialView;
    private static ImageButton tutorialText;
    private static ImageView tutorialArrow;
    private static int xSlot;
    private static boolean fadingIn;

    private TutorialManager() {}

    public static void init(Activity activity, InterceptingFrameLayout click) {
        tutorialView = activity.findViewById(R.id.tutorial);
        tutorialText = (ImageButton) activity.findViewById(R.id.tutorial_text);
        tutorialArrow = (ImageView) activity.findViewById(R.id.tutorial_arrow);

        click.setOnInterceptTouchEventListener(new InterceptingFrameLayout.OnInterceptTouchEventListener() {
            @Override
            public boolean onInterceptTouchEvent(InterceptingFrameLayout view, MotionEvent ev, boolean disallowIntercept) {
                if (ev.getAction() == MotionEvent.ACTION_DOWN) {
                    if (tutorialView.getAlpha() == 1.0f || fadingIn) {
                        hideTutorial();
                    }
                }

                return false;
            }

            @Override
            public boolean onTouchEvent(InterceptingFrameLayout view, MotionEvent event) {
                return false;
            }
        });

        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        xSlot = (width / 7);
    }

    private static void hideTutorial() {
        fadingIn = false;
        tutorialView.clearAnimation();

        tutorialView.animate().alpha(0f).setDuration((long) (500 * tutorialView.getAlpha())).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                tutorialView.setAlpha(0f);
            }
        });
    }

    public static void showTutorial(int tutorialRes, int pos) {
        // TODO TUTORIAL tutorialText.setImageResource(tutorialRes);
        fadingIn = true;

        tutorialArrow.setX((xSlot / 2) - (tutorialArrow.getDrawable().getIntrinsicWidth() / 2) + (xSlot * pos));
        tutorialView.clearAnimation();
        tutorialView.animate().alpha(1f).setDuration((long) (500 * (1 - tutorialView.getAlpha()))).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                tutorialView.setAlpha(1f);
                fadingIn = false;
            }
        });
    }
}
