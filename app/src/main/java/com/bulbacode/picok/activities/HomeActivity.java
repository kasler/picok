package com.bulbacode.picok.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bulbacode.picok.BuildConfig;
import com.bulbacode.picok.R;
import com.bulbacode.picok.utils.ImageCaptureUtils;
import com.bulbacode.picok.utils.ImageFileProperties;
import com.bulbacode.picok.utils.IntentUtils;
import com.bulbacode.picok.utils.LogUtils;
import com.bulbacode.picok.utils.PermissionManager;

import java.io.IOException;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageButton;

/**
 * HomeActivity is the first screen, showing two buttons to: a capture camera option,
 * gallery picker and a list with different options
 */

public class HomeActivity extends Activity implements Animation.AnimationListener {

    private static final int FACEBOOK = 0;
    private static final int INSTAGRAM = 1;
    private static final int TUTORIAL = 2;
    private static final int SHARE = 3;
    private static final int RATE = 4;
    private static final int CONTACT = 5;

    private static final String TAG = HomeActivity.class.getSimpleName();

    // TODO replace with real username
    private static final String PICOK_INSTAGRAM_USERNAME = "sierrapruitt";
    // TODO replace with real username
    private static final String PICOK_FACEBOOK_USERNAME = "nba/?fref=ts";
    // TODO replace with real URL
    private static final String PICOK_FACEBOOK_URL = "https://www.facebook.com/nba/?fref=ts";
    private static final String FACEBOOK_URI = "fb://facewebmodal/f?href=";
    private static final String FACEBOOK_URL = "https://www.facebook.com/";

    private static final String INSTAGRAM_URI = "http://instagram.com/_u/";
    private static final String INSTAGRAM_URL = "http://instagram.com/";


    private static final String facebookFullURI = FACEBOOK_URI + PICOK_FACEBOOK_URL;
    private static final String facebookFullUrl = FACEBOOK_URL + PICOK_FACEBOOK_USERNAME;
    private static final String instagramFullURI = INSTAGRAM_URI + PICOK_INSTAGRAM_USERNAME;
    private static final String instagramFullUrl = INSTAGRAM_URL + PICOK_INSTAGRAM_USERNAME;

    public static final int IMAGE_QUALITY = 70;

    private static final String PACKAGE_NAME = "com.miniclip.agar.io";
    private static final String DEFAULT_MARKER_STRING = "market://details?id="  ;
    private static final int DEFAULT_WIDTH = 400;
    private static final int DEFAULT_HEIGHT = 400;
    private static final double DELTA = 0.3;

    private LinearLayout upperLayout;
    private ListView actionList;

    private GifImageButton actionsButton;
    private GifDrawable menuInGif;
    private GifDrawable menuOutGif;

    private ImageButton galleryButton;
    private ImageButton cameraButton;

    private Animation animSlideDown;
    private Animation animSlideUp;

    private boolean isListVisible = false;

    public static final String IS_GALLERY = "IS_GALLERY";
    public static final String URI = "URI";

    private String currentPhotoPath;
    private String compressedPhotoPath;

    private long lastTimePressed = System.nanoTime();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        settingsGifs();

//        FlurryManager.getInstance().startApplicationEvent();
        // all item options
        final ItemList[] items = {
                new ItemList(getString(R.string.facebook_action), android.R.drawable.ic_menu_delete),
                new ItemList(getString(R.string.instagram_action), 0),//no icon for this one
                new ItemList(getString(R.string.tutorial_action), 0),//no icon for this one
                new ItemList(getString(R.string.share_action), 0),//no icon for this one
                new ItemList(getString(R.string.rate_action), 0),//no icon for this one
                new ItemList(getString(R.string.contact_action), android.R.drawable.ic_menu_add),

        };

        actionList = (ListView) findViewById(R.id.actionList);
        CustomAdapter adapter = new CustomAdapter(this, items);
        actionList.setAdapter(adapter);
        actionList.setOnItemClickListener(adapter);

        galleryButton = (ImageButton) findViewById(R.id.galleryBtn);
        cameraButton = (ImageButton) findViewById(R.id.cameraBtn);

        animSlideDown = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down);
        animSlideUp = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_up);

        animSlideDown.setAnimationListener(this);
        animSlideUp.setAnimationListener(this);

        actionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.w(TAG, "actionBtn onClick()");
                long currentTime = System.nanoTime();
                long delta = currentTime - lastTimePressed;
                double deltaSeconds = (double) delta/ 1000000000.0;

                if ( deltaSeconds >= DELTA) {
                    GifDrawable gifDrawable = (GifDrawable) actionsButton.getDrawable();
                    gifDrawable.reset();

                    if (isListVisible) {
                        actionsButton.setImageDrawable(menuOutGif);
                        closeListAnimation();
                    }
                    else { // invisible
                        actionsButton.setImageDrawable(menuInGif);
                        loadListAnimation();
                    }

                    isListVisible = !isListVisible;
                    gifDrawable = (GifDrawable) actionsButton.getDrawable();
                    gifDrawable.reset();
                    gifDrawable.start();

                    lastTimePressed = currentTime;
                }


            }
        });

        galleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startGallery();

            }
        });
        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startCamera();
            }
        });

        upperLayout = (LinearLayout) findViewById(R.id.upper_layout);

//        measuredHeight = actionList.getMeasuredHeight();
        // TODO list click listener
        PermissionManager.checkWritePermission(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void settingsGifs() {
        try {
            menuOutGif = new GifDrawable( getResources(), R.drawable.menu_out );
        } catch (IOException e) {
            e.printStackTrace();
        }


        actionsButton = (GifImageButton) findViewById(R.id.actionsBtn);
        menuInGif = (GifDrawable) actionsButton.getDrawable();
        menuInGif.stop();

        menuOutGif.setSpeed(2.0f);
        menuInGif.setSpeed(2.0f);
    }

    private void loadListAnimation() {
        cancelPreviousAnimation();

        actionList.setVisibility(View.VISIBLE);
        actionList.clearAnimation();
        actionList.startAnimation(animSlideDown);
    }

    private void closeListAnimation() {
        cancelPreviousAnimation();

        actionList.clearAnimation();
        actionList.startAnimation(animSlideUp);
        actionList.setVisibility(View.GONE);
    }

    private void cancelPreviousAnimation() {
        Animation animation = actionList.getAnimation();
        if (animation != null && animation.hasStarted()) {
            animation.cancel();
        }
    }


    private void startGallery() {

        if (Build.VERSION.SDK_INT >= 23) {
            if (PermissionManager.isReadStorageAllowed(this)) {
                startGalleryIntent();
            }
            else {
                PermissionManager.requestStoragePermission(this, PermissionManager.REQUEST_WRITE_STORAGE_GALLERY);
            }
        }
        else {
            startGalleryIntent();
        }


    }

    private void startGalleryIntent() {
        ImageFileProperties imageFileProperties = ImageCaptureUtils.dispatchStartGalleryImagePicker(HomeActivity.this);
        currentPhotoPath = imageFileProperties.path;
        compressedPhotoPath = imageFileProperties.compressedPath;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        boolean ret = super.dispatchTouchEvent(ev);

        if (actionList.getVisibility() == View.VISIBLE) {
            Log.d(TAG, "is list activated:" + actionList.isActivated());

            actionsButton.callOnClick();
        }

        return ret;

    }

    @Override
    public void onAnimationStart(Animation animation) {
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if (animation.equals(animSlideUp)) {
            actionList.setVisibility(View.INVISIBLE);
            isListVisible = false;
        }
        else {
            isListVisible = true;
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
        Log.d(TAG, "animationRepeat()");
    }


    private void startCamera() {
        // check permission for writing on external disk our file manipulations

        if (Build.VERSION.SDK_INT >= 23) {
            if (PermissionManager.isReadStorageAllowed(this)) {
                startCameraIntent();
            }
            else {
                PermissionManager.requestStoragePermission(this, PermissionManager.REQUEST_WRITE_STORAGE_CAMERA);
            }
        }
        else {
            startCameraIntent();
        }


    }

    private void startCameraIntent() {
        ImageFileProperties imageFileProperties = ImageCaptureUtils.dispatchCameraIntent(this);
        currentPhotoPath = imageFileProperties.path;
        compressedPhotoPath = imageFileProperties.compressedPath;
        LogUtils.printString(TAG, currentPhotoPath, "currentPhotoPath");
    }



    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        if (currentPhotoPath != null) {
            savedInstanceState.putString("filePath", currentPhotoPath);
            savedInstanceState.putString("compressedFilePath", compressedPhotoPath);
        }

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState.getString("filePath") != null) {
            currentPhotoPath = savedInstanceState.getString("filePath");
            compressedPhotoPath = savedInstanceState.getString("compressedFilePath");
        }

        // Always call the superclass so it can restore the view hierarchy
        super.onRestoreInstanceState(savedInstanceState);
    }


    private void rateGooglePlay() {
        Intent goToMarketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(DEFAULT_MARKER_STRING + PACKAGE_NAME));

        if (!IntentUtils.isIntentAvailable(this, goToMarketIntent)) {
            goToMarketIntent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + PACKAGE_NAME));
        }

        addBackTraceIntentLogic(goToMarketIntent);

        startActivity(goToMarketIntent);
    }

    private void addBackTraceIntentLogic(Intent intent) {
        // This will allow the user to go back to our app
        // if he presses the back button TODO try to replace with FLAG_ACTIVITY_NEW_TASK
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
    }

    // is called before we forward our user to another application
    private void startAPIIntent(String intentUri, String webUrl, String packageName) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(intentUri));
        intent.setPackage(packageName);

        if (!IntentUtils.isIntentAvailable(this, intent)){
            // will open the browser if the application doesn't exist
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(webUrl));
        }

        addBackTraceIntentLogic(intent);

        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d(TAG, "onActivityResult() request code:" + requestCode);
        boolean isImageSet = false;

        switch (requestCode) {
            case ImageCaptureUtils.PICK_IMAGE_REQUEST:
                if (resultCode == RESULT_OK && data != null && data.getData() != null) {

                    currentPhotoPath = ImageCaptureUtils.getUriFromGalleryImage(data, getContentResolver());
                    if (currentPhotoPath != null && !currentPhotoPath.isEmpty()) {
                        isImageSet = true;
                    }
                    else {
                        if (BuildConfig.DEBUG) {
                            Toast.makeText(HomeActivity.this, "image not saved", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                else if (BuildConfig.DEBUG && resultCode == RESULT_CANCELED){
                    Log.i(TAG, "onActivityResult() cancelled");
                }
                else {
                    if (BuildConfig.DEBUG) {
                        Log.w(TAG, "onActivityResult() gallery - no image. resultCode:" + resultCode);
                    }
                }
                break;

            case ImageCaptureUtils.CAMERA_IMAGE:
                Log.d(TAG, "Camera image");
                if (resultCode == RESULT_OK ) {
                    Log.i(TAG, "onActivityResult() camera ok");
                    Log.d(TAG, "file image byte size:" + currentPhotoPath.getBytes());
                    Log.d(TAG, "file image path:" + currentPhotoPath);

                    ImageCaptureUtils.galleryAddPic(this, currentPhotoPath);

                    ImageCaptureUtils.getUriFromCameraImage(currentPhotoPath);
                    isImageSet = true;
                }
                else if (resultCode == RESULT_CANCELED) {
                    Log.i(TAG, "onActivityResult() camera cancelled");
                }
                else {
                    Log.w(TAG, "onActivityResult() camera - no image. resultCode:" + resultCode);
                }
                break;
        } // end switch

        if (isImageSet) {
            String path = currentPhotoPath;
            String compressedPath = compressedPhotoPath;

            Bitmap.CompressFormat compressFormat = Bitmap.CompressFormat.JPEG;
            ImageCaptureUtils.decodeBitmapFromFile(DEFAULT_WIDTH, DEFAULT_HEIGHT, path, compressedPath, IMAGE_QUALITY, compressFormat);
            Uri uri = ImageCaptureUtils.getUriFromCameraImage(compressedPath);

            startCropActivity(uri, false);
        }
    }



    private void startCropActivity(Uri uri, boolean isGallery) {
        Intent intent = new Intent(this, CropActivity.class);
        intent.putExtra(URI, uri);
        intent.putExtra(IS_GALLERY, isGallery);
        startActivity(intent);
    }

    // inner class for a list item
    public static class ItemList {
        public final String text;
        public final int icon;

        public ItemList(String text, Integer icon) {
            this.text = text;
            this.icon = icon;
        }
        @Override
        public String toString() {
            return text;
        }
    }


    private class CustomAdapter extends ArrayAdapter<ItemList> implements ListView.OnItemClickListener{

        private final Activity context;
        private final ItemList[] items;

        public CustomAdapter(Activity context, ItemList[] items) {
            super(context, R.layout.list_item, items);

            this.context = context;
            this.items = items;
        }

        public View getView(int position,View view,ViewGroup parent) {
            LayoutInflater inflater = context.getLayoutInflater();
            View rowView = inflater.inflate(R.layout.list_item, null, true);

            ImageView imageView = (ImageView) rowView.findViewById(R.id.shape_button);
            TextView txtTitle = (TextView) rowView.findViewById(R.id.item_name);

            // setting the view height according to the parent view
            int listViewMeasuredHeight = parent.getMeasuredHeight();
            Log.d(TAG, "measured height: " + listViewMeasuredHeight);

            int listViewPadding = 10;
            int listItemHeight = ((listViewMeasuredHeight ) / items.length - listViewPadding);

            Log.d(TAG, "listView padding: " + listViewPadding);
            Log.d(TAG, "list item height: " + listItemHeight);

            imageView.getLayoutParams().height = listItemHeight;
            txtTitle.getLayoutParams().height = listItemHeight;

            txtTitle.setText(items[position].text);
            imageView.setImageResource(items[position].icon);

            return rowView;

        };

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Log.d(TAG, "listView onClick()");

            switch (i) {
                case FACEBOOK:
                    startAPIIntent(facebookFullURI, facebookFullUrl, IntentUtils.FACEBOOK_PACKAGE_NAME);
                    break;
                case INSTAGRAM:
                    startAPIIntent(instagramFullURI, instagramFullUrl, IntentUtils.INSTAGRAM_PACKAGE_NAME);
                    break;
                case TUTORIAL:
//                    TODO add tutorial when UI is done
                    break;
                case SHARE:
//                    TODO add share when Facebook/Instagram page is ready
                    break;
                case RATE:
                    rateGooglePlay();
                case CONTACT:
//                    TODO add contanct when UI is done
                    break;
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PermissionManager.REQUEST_WRITE_STORAGE_CAMERA:
                if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
                    Log.v(TAG,"Permission: "+permissions[0]+ "was "+grantResults[0]);

                    startCameraIntent();
                }
                else {
                    Log.w(TAG, "Write permission not granted");
                }
                break;
            case PermissionManager.REQUEST_WRITE_STORAGE_GALLERY:
                if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
                    Log.v(TAG,"Permission: "+permissions[0]+ "was "+grantResults[0]);

                    startGallery();
                }
                else {
                    Log.w(TAG, "Write permission not granted");
                }
                break;
        }
    }
}
