package com.bulbacode.picok.activities;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bulbacode.picok.R;

public class TestColorActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        final ImageView imageView = (ImageView) findViewById(R.id.testImageView);

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.nature6);
        Bitmap workingBitmap = Bitmap.createBitmap(bitmap);
        Bitmap mutableBitmap = workingBitmap.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(mutableBitmap);
//        bitmapDrawable.setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_ATOP);
//        bitmapDrawable.invalidateSelf();
        Paint paint = new Paint();
        ColorFilter filter = new PorterDuffColorFilter(Color.CYAN, PorterDuff.Mode.SRC_ATOP);
        paint.setColorFilter(filter);



        canvas.drawBitmap(mutableBitmap, 0, 0, paint);

        imageView.setImageBitmap(mutableBitmap);

        Button button = (Button) findViewById(R.id.testATOP);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
            }
        });


        findViewById(R.id.testLIGHTEN).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.setColorFilter(Color.BLACK, PorterDuff.Mode.LIGHTEN);
            }
        });

        findViewById(R.id.testMULT).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView.setColorFilter(Color.CYAN, PorterDuff.Mode.MULTIPLY);
            }
        });
    }
}
