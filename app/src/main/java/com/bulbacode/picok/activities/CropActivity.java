package com.bulbacode.picok.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bulbacode.picok.R;
import com.bulbacode.picok.utils.ImageCaptureUtils;
import com.bulbacode.picok.utils.PermissionManager;
import com.naver.android.helloyako.imagecrop.view.ImageCropView;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageButton;

/**
 * CropActivity is responsible to crop pictures on demand from
 * HomeActivity and EditActivity
 */

public class CropActivity extends Activity {

    private static final String TAG = CropActivity.class.getSimpleName();
    private static final int IMAGE_QUALITY = 80;
    private ImageView imageView;
    private Bitmap originalBitmap;
    private ImageCropView cropperView;
    private ImageButton backBtn;
    private GifImageButton doneButton;
    private GifDrawable doneGif;
    private TextView titleActivity;
    private ImageView testImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate()");
        setContentView(R.layout.activity_crop);

        cropperView = (ImageCropView) findViewById(R.id.cropper_view);
        backBtn = (ImageButton) findViewById(R.id.back_button);
        doneButton = (GifImageButton) findViewById(R.id.done_button);
        doneGif = (GifDrawable) doneButton.getDrawable();
        doneGif.stop();

        titleActivity = (TextView) findViewById(R.id.activity_title);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                done();

            }
        });

//        setCropperWidthHeight();

        Intent intent = getIntent();
        if (intent == null) {
            Log.e(TAG, "error: intent from HomeActivity is null");
        }
        else {
            originalBitmap = null;
            Uri uri = intent.getParcelableExtra(HomeActivity.URI);

            if (uri != null) {
                // TODO decode the bitmap with inSample
                originalBitmap = ImageCaptureUtils.getBitmapFromURI(uri, getContentResolver());

            }
            else {
                // in test mode, setting a default picture
                originalBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.nature7);
            }

            // Set Bitmap
            cropperView.setImageBitmap(originalBitmap);

            cropperView.setAspectRatio(1, 1);

//            cropperView.zoomTo(100f, 100f);

            // Set Max and Min Cropping

            // set centerCrop

            // Fit image to center
            PermissionManager.checkWritePermission(this);
        }
    }

    private void done() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (PermissionManager.isReadStorageAllowed(this)) {
                this.crop();
            }
            else {
                PermissionManager.requestStoragePermission(this, PermissionManager.REQUEST_WRITE_STORAGE_GENERAL);
            }
        }
        else { // regular permission model
            this.crop();
        }

    }

    private void crop() {
        doneGif.start();
        Intent intent;
        Bitmap bitmap = cropperView.getCroppedImage();
        Bitmap.CompressFormat compress = Bitmap.CompressFormat.PNG;
        Uri uri = ImageCaptureUtils.getUriFromBitmap(bitmap, IMAGE_QUALITY, compress);

        // check to see if another activity has asked a result
        if (uri != null) {
            int request = getIntent().getIntExtra(EditActivity.CROP_RETURN, 0);
            if (request == EditActivity.CROP_RETURN_REQUEST) { // started from EditActivity
                intent = new Intent();
                intent.putExtra("uri", uri);
                setResult(RESULT_OK, intent);
                finish();
            }
            else { // start EditActivity with the cropped result
                intent = new Intent(CropActivity.this, EditActivity.class);
                intent.putExtra("uri", uri);
                startActivity(intent);
            }

        }
        else {
            Toast.makeText(CropActivity.this, "failed to create a new image file, try again", Toast.LENGTH_SHORT).show();
        }
    }

//    // we want the cropper to be a perfect square
//    private void setCropperWidthHeight() {
//        Display display = getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int width = size.x;
//        int height = size.y;
//
//        Log.d(TAG, "width screen:" + width);
//        Log.d(TAG, "height screen:" + height);
//
//        Log.d(TAG, "cropper width:" + cropperView.getLayoutParams().width);
//        Log.d(TAG, "cropper height:" + cropperView.getLayoutParams().height);
//
//        cropperView.getLayoutParams().width = width;
//        cropperView.getLayoutParams().height = width;
//
//        Log.d(TAG, "cropper width:" + cropperView.getLayoutParams().width);
//        Log.d(TAG, "cropper height:" + cropperView.getLayoutParams().height);
//    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


    }
}
