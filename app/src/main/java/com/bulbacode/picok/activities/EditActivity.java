package com.bulbacode.picok.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.bulbacode.picok.AdManager;
import com.bulbacode.picok.BuildConfig;
import com.bulbacode.picok.MyApplication;
import com.bulbacode.picok.R;
import com.bulbacode.picok.WorkView;
import com.bulbacode.picok.utils.FlurryManager;
import com.bulbacode.picok.utils.ImageCaptureUtils;
import com.bulbacode.picok.utils.ImageFileProperties;
import com.bulbacode.picok.utils.InterceptingFrameLayout;
import com.bulbacode.picok.utils.PermissionManager;
import com.bulbacode.picok.utils.PicokImageWorker;
import com.bulbacode.picok.worktools.ShapeTool;
import com.bulbacode.picok.worktools.ShareWorkTool;
import com.bulbacode.picok.worktools.WorkTools;
import com.example.android.displayingbitmaps.util.ImageCache;

import java.io.IOException;
import java.util.Random;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageButton;

/**
 *
 * EditActivity is the editing screen
 */


public class EditActivity extends FragmentActivity {

    public static final int CROP_RETURN_REQUEST = 300;
    public static final String CROP_RETURN = "CROP_RETURN";

    private static final String IMAGE_CACHE_DIR = "thumbs";

    private static final int IMAGE_QUALITY = 100;
    private static final String TAG = EditActivity.class.getSimpleName();
    public static final int REQ_WIDTH = 800;
    public static final int REQ_HEIGHT = 400;

    private static Activity activity;

    public static int screenWidth;
    public static int screenHeight;

    private ShareWorkTool shareWorkTool;
    private FrameLayout photoFrame;
    private AdManager adManager;

    public static PicokImageWorker picokImageWorker;
    private TutorialManager tutorialManager;

    private GifImageButton saveButton;
    private GifDrawable gifFromResource;
    private String filePath;
    private String compressedFilePath;
    private BitmapDrawable bitmapDrawable1;
    private BitmapDrawable bitmapDrawable2;
    private BitmapDrawable bitmapDrawable3;

    //protected onSav

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        // regular size ad

        // TODO add before sending apk
//        AdView mAdView = (AdView) findViewById(R.id.ad_view);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);

        //hideNavbarOnCreate();

        ImageCache.ImageCacheParams cacheParams =
                new ImageCache.ImageCacheParams(this, IMAGE_CACHE_DIR);

        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        picokImageWorker = new PicokImageWorker(this);
        picokImageWorker.setLoadingImage(R.drawable.ic_launcher);
        picokImageWorker.addImageCache(this.getSupportFragmentManager(), cacheParams);

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);


        screenHeight = displaymetrics.heightPixels;
        screenWidth = displaymetrics.widthPixels;

        activity = this;

        initViews();

        // convert URI to bitmap
        Intent intentFromCrop = getIntent();
        Pair<Bitmap, String> bitmapPair1 = getBitampFromIntent(intentFromCrop);
        Pair<Bitmap, String> bitmapPair2 = null;
        Pair<Bitmap, String> bitmapPair3 = null;

        if (bitmapPair1 == null) { // probably starting the application from EditActivity due to testing
            Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.nature6);
//            Bitmap bitmap1 = decodeSampledBitmapFromResource(getResources(), R.drawable.nature6, 100, 100);
//            Bitmap bitmap2 = Bitmap.createScaledBitmap(bitmap1, REQ_WIDTH, REQ_HEIGHT, false);

            bitmapPair1 = new Pair<>(bitmap1, "");
            bitmapPair2 = bitmapPair1;
            bitmapPair3 = bitmapPair1;

        }
        else {
            Bitmap bitmap1 = bitmapPair1.first;
//            bitmap1 = Bitmap.createScaledBitmap(bitmap1, REQ_WIDTH, REQ_HEIGHT, false);
            bitmapPair1 = new Pair<>(bitmap1, "");
            bitmapPair2 = bitmapPair1;
            bitmapPair3 = bitmapPair1;
        }


        //add bitmaps with keys to memCache

//        BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), bitmapPair.first);

//        String originalBitmapKey = bitmapPair.second;
        bitmapDrawable1 = new BitmapDrawable(getResources(), bitmapPair1.first);
        bitmapDrawable2 = new BitmapDrawable(getResources(), bitmapPair2.first);
        bitmapDrawable3 = new BitmapDrawable(getResources(), bitmapPair3.first);


        Random rand = new Random();

        String firstBitmapKey = String.valueOf(rand.nextInt(1000000));
        final String secondBitmapKey = String.valueOf(rand.nextInt(1000000));
        String thirdBitmapKey = String.valueOf(rand.nextInt(1000000));

        getImageCache().addBitmapToCache(firstBitmapKey, bitmapDrawable1);
        getImageCache().addBitmapToCache(secondBitmapKey, bitmapDrawable2);
        getImageCache().addBitmapToCache(thirdBitmapKey, bitmapDrawable3);

        WorkView.getInstance().init(firstBitmapKey, secondBitmapKey, thirdBitmapKey);

        TutorialManager.init(this, (InterceptingFrameLayout) findViewById(R.id.anything));

        WorkTools.getInstance().init();

        adManager = new AdManager();

        // dynamically determine width and height
        photoFrame = (FrameLayout) findViewById(R.id.main_view_layout);

        photoFrame.getLayoutParams().height = screenWidth;
        photoFrame.getLayoutParams().width = screenWidth;

//        photoFrame.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
//            @Override
//            public void onGlobalLayout() {
//                photoFrame.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//                WorkView.getInstance().getLower().getView().setVisibility(View.VISIBLE);
//                WorkView.getInstance().getLower().loadNewImage(secondBitmapKey, false);
//                WorkView.getInstance()
//            }
//        });

        FlurryManager.getInstance().startDesignSession();

        try {
            gifFromResource = new GifDrawable(getResources(), R.drawable.done2save);
        } catch (IOException e) {
            e.printStackTrace();
        }

        PermissionManager.checkWritePermission(this);
    }


    private void initViews() {
        ImageButton backButton = (ImageButton) findViewById(R.id.edit_back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        saveButton = (GifImageButton) findViewById(R.id.edit_save_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSavePressed();
            }
        });

    }

    protected void onStop() {
        super.onStop();
    }

    private void onSavePressed() {

        if (Build.VERSION.SDK_INT >= 23) {
             MyApplication myApplication = (MyApplication) getApplicationContext();
            if (myApplication.writePermission == true) {
                doSave();
            }
            else {
                PermissionManager.requestStoragePermission(this, PermissionManager.REQUEST_WRITE_STORAGE_GENERAL);
            }
        }
        else {
            doSave();
        }

     }

    private void doSave() {
        Bitmap bitmap = ImageCaptureUtils.getViewBitmap(photoFrame);
        Bitmap.CompressFormat compressFormat = Bitmap.CompressFormat.PNG;
        Uri uri = ImageCaptureUtils.getUriFromBitmap(bitmap, IMAGE_QUALITY, compressFormat);
        shareWorkTool  = new ShareWorkTool(this, uri);
        WorkTools.getInstance().switchToolLayout(shareWorkTool);

        // gif animation
        // setting a new gif drawable
        if (gifFromResource != null) {
            saveButton.setImageDrawable(gifFromResource);
        }
        else {
            Log.e(TAG, "gif is null");
        }

        saveButton.setBackgroundResource(R.drawable.done2save);

        FlurryManager.getInstance().endSession();
    }


    public void onBackPressed() {
        picokImageWorker.clearCache();
        ShapeTool.selectedShapeFamily = null;
        WorkTools.delete();
        finish();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        if (WorkTools.getInstance().getChangeImageTool().getImageFileProperties() != null) {
            savedInstanceState.putString("filePath", WorkTools.getInstance().getChangeImageTool().getImageFileProperties().path);
            savedInstanceState.putString("compressedFilePath", WorkTools.getInstance().getChangeImageTool().getImageFileProperties().compressedPath);
        }

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {

        this.filePath = savedInstanceState.getString("filePath");
        this.compressedFilePath = savedInstanceState.getString("compressedFilePath");

        // Always call the superclass so it can restore the view hierarchy
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d(TAG, "onActivityResult() request code:" + requestCode);
        ImageFileProperties imageFileProperties = WorkTools.getInstance().getChangeImageTool().getImageFileProperties();

        if (imageFileProperties == null)
            imageFileProperties = new ImageFileProperties(null, this.filePath, this.compressedFilePath, null);

        String currentPhotoPath = imageFileProperties.path;

        boolean isImage = false;
        switch (requestCode) {
            case ImageCaptureUtils.CAMERA_IMAGE:
                Log.d(TAG, "Camera image");
                if (resultCode == RESULT_OK ) {
                    ImageCaptureUtils.galleryAddPic(this, currentPhotoPath);
                    isImage = true;
                }
                else if (resultCode == RESULT_CANCELED) {
                    Log.i(TAG, "onActivityResult() camera cancelled");
                }
                else {
                    Log.w(TAG, "onActivityResult() camera - no image. resultCode:" + resultCode);
                }
                break; // end camera case

            case ImageCaptureUtils.PICK_IMAGE_REQUEST:
                if (resultCode == RESULT_OK && data != null && data.getData() != null) {

                    imageFileProperties.path = ImageCaptureUtils.getUriFromGalleryImage(data, getContentResolver());
                    if (imageFileProperties.path != null && !imageFileProperties.path.isEmpty()) {
                        isImage = true;
                    }
                }
                else if (BuildConfig.DEBUG && resultCode == RESULT_CANCELED){
                    Log.i(TAG, "onActivityResult() cancelled");
                }
                else {
                    if (BuildConfig.DEBUG) {
                        Log.w(TAG, "onActivityResult() gallery - no image. resultCode:" + resultCode);
                    }
                }
                break; // end gallery case

            case CROP_RETURN_REQUEST: {
                if (RESULT_OK == resultCode) {
                    Log.d(TAG, "onActivityResult() crop_return");
                    Pair<Bitmap, String> bitmapPair = getBitampFromIntent(data);
                    //if (compressedFilePath.equals(bitmapPair.second)) {
                      //  Log.d(TAG, "wrong");
                    //}
                    getImageCache().addBitmapToCache(bitmapPair.second, new BitmapDrawable(getResources(), bitmapPair.first));
                    WorkView.getInstance().changeImage(imageFileProperties.isInner, bitmapPair.second);
                }
                break;
            } // end crop case
        } // end switch

        // after we got a picture from gallery/camera we send it to the crop activity
        if (isImage) {
//          TODO decide dynamically about REQ_WIDTH and REQ_HEIGHT

            Bitmap.CompressFormat compressFormat = Bitmap.CompressFormat.JPEG;
            ImageCaptureUtils.decodeBitmapFromFile(REQ_WIDTH, REQ_HEIGHT, imageFileProperties.path, imageFileProperties.compressedPath, IMAGE_QUALITY, compressFormat);
            Uri uri = ImageCaptureUtils.getUriFromCameraImage(imageFileProperties.compressedPath);
            forwardBitmapURIToCrop(uri);
        }
    }

    public static Activity getActivity() {
        return activity;
    }

    private Pair<Bitmap, String> getBitampFromIntent(Intent intent) {
        Pair<Bitmap, String> bitmapPath = null;
        if (intent.hasExtra("uri")) {
            Uri uri = intent.getParcelableExtra("uri");
            Bitmap bitmap = ImageCaptureUtils.getBitmapFromURI(uri, getContentResolver());
            bitmapPath = new Pair<>(bitmap, uri.getPath());
        }
        return bitmapPath;
    }

    private void forwardBitmapURIToCrop(Uri uri) {
        Intent intent = new Intent(this, CropActivity.class);
        intent.putExtra("URI", uri);
        intent.putExtra("IS_GALLERY", false);
        intent.putExtra(CROP_RETURN, EditActivity.CROP_RETURN_REQUEST);
        startActivityForResult(intent, EditActivity.CROP_RETURN_REQUEST);
    }

    public static ImageCache getImageCache() {
        return picokImageWorker.getmImageCache();
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();

        Log.e(TAG, "low memory !!!!");
    }

    //Load a bitmap from a resource with a target size
    static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    //Given the bitmap size and View size calculate a subsampling size (powers of 2)
    static int calculateInSampleSize( BitmapFactory.Options options, int reqWidth, int reqHeight) {
        int inSampleSize = 1;	//Default subsampling size
        // See if image raw height and width is bigger than that of required view
        if (options.outHeight > reqHeight || options.outWidth > reqWidth) {
            //bigger
            final int halfHeight = options.outHeight / 2;
            final int halfWidth = options.outWidth / 2;
            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        MyApplication application = (MyApplication) getApplicationContext();

        switch (requestCode) {
            case PermissionManager.REQUEST_WRITE_STORAGE_GENERAL:
                if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
                    Log.v(TAG,"Permission: "+permissions[0]+ "was "+grantResults[0]);

                    application.writePermission = true;

                }
                else {
                    Log.w(TAG, "Write permission not granted");
                    application.writePermission = false;
                }
                break;
        }

    }
}