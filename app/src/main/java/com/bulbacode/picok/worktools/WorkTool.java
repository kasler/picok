package com.bulbacode.picok.worktools;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;

import com.bulbacode.picok.activities.EditActivity;
import com.bulbacode.picok.activities.TutorialManager;

/**
 * Created by Hadadi on 1/5/2016.
 *
 * The base of a tool (like blur, setZoom..)
 */
public abstract class WorkTool /* extends Fragment*/ implements View.OnClickListener {
    // the button above the frame



    private final ImageButton toolButton;
    protected View toolView;
    private int toolMainViewRes;
    protected int inactiveButtonResource;
    protected int activeButtonResource;
    protected boolean isActive;
    private boolean isAlive;


    public WorkTool(int toolLayoutRes,
                    int inactiveButtonResource,
                    int activeButtonResource,
                    int toolButtonResource) {

        this.toolMainViewRes = toolLayoutRes;
        this.inactiveButtonResource = inactiveButtonResource;
        this.activeButtonResource = activeButtonResource;
        this.toolButton = (ImageButton) EditActivity.getActivity().findViewById(toolButtonResource);

        this.isActive = false;

        LayoutInflater inflater = EditActivity.getActivity().getLayoutInflater();
        this.toolView = inflater.inflate(this.toolMainViewRes, null);

        // this.toolMainView = EditActivity.getActivity().findViewById(this.toolMainViewRes);
        if (toolButton != null) {
            this.toolButton.setImageResource(inactiveButtonResource);
            toolButton.setOnClickListener(this);
        }
        // this.toolView = EditActivity.getActivity().findViewById(this.toolMainViewRes);

    }

    /*public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.toolView = inflater.inflate(this.toolMainViewRes, container, false);

        return this.toolView;
    }*/

    @Override
    public void onClick(View view) {
        WorkTools.getInstance().switchToolLayout(this);
    }

    public void activate() {
        if (this.isActive)
            return;

        // might be null
        if (this.toolButton != null) {
            this.toolButton.setImageResource(this.activeButtonResource);
        }
        this.toolView.setVisibility(View.VISIBLE);

        this.isActive = true;

        onActivate();

        // If we were already alive, this activation is a onResume
        if (this.isAlive)
            onResume();
        else {
            // Show tutorial
            TutorialManager.showTutorial(0, 5);
            this.isAlive = true;
        }
    }

    protected void onActivate() {
        // Activation code for whoever wants to
    }

    protected void onResume() {
        // Resume code for whoever wants to
    }

    protected void deactivate() {
        if (!this.isActive)
            return;

        // might be null
        if (this.toolButton != null) {
            this.toolButton.setImageResource(this.inactiveButtonResource);
        }

        this.toolView.setVisibility(View.GONE);

        this.isActive = false;
    }

    public View getMainView() {
        return this.toolView;
    }
}