package com.bulbacode.picok.worktools;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.bulbacode.picok.R;
import com.bulbacode.picok.utils.FlurryManager;
import com.bulbacode.picok.utils.IntentUtils;

/**
 * Created by sergey on 2/1/16.
 */
public class ShareWorkTool extends WorkTool {
    private Activity activity;
    private Uri bitmapUri;

    private static String type = "image/*";


    public enum ShareMethod {
        INSTAGRAM,
        FACEBOOK,
        ANDROID_GENERAL
    }

    public ShareWorkTool(final Activity activity, Uri uri) {
        super(R.layout.share_work_tool_layout, 0, 0, 0);

        this.activity = activity;
        this.bitmapUri = uri;

        View mainView = this.getMainView();

        ImageButton shareInstagramBtn = (ImageButton) mainView.findViewById(R.id.instagramShareBtn);
        ImageButton facebookShareBtn = (ImageButton) mainView.findViewById(R.id.facebookShareBtn);
        ImageButton androidShareBtn = (ImageButton) mainView.findViewById(R.id.androidShareBtn);

        shareInstagramBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                instagram();
            }
        });

        facebookShareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                facebook();
            }
        });

        androidShareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareAndroid(type, bitmapUri);
            }
        });
    }

    private void facebook() {
        FlurryManager.getInstance().shareEvent(ShareMethod.FACEBOOK.name());
        createExplicitAPIIntent(type, bitmapUri, IntentUtils.FACEBOOK_PACKAGE_NAME, "Facebook");

    }

    private void shareAndroid(String type, Uri uri) {
        FlurryManager.getInstance().shareEvent(ShareMethod.ANDROID_GENERAL.name());
        // Create the new Intent using the 'Send' action.

        Intent share = new Intent(Intent.ACTION_SEND);

        // Set the MIME type
        share.setType(type);

        // Add the URI to the Intent.
        share.putExtra(Intent.EXTRA_STREAM, bitmapUri);

        // Broadcast the Intent.
        activity.startActivity(Intent.createChooser(share, "Share to"));
    }

    private void instagram() {
        FlurryManager.getInstance().shareEvent(ShareMethod.INSTAGRAM.name());
        createExplicitAPIIntent(type, bitmapUri, IntentUtils.INSTAGRAM_PACKAGE_NAME, "Instagram");
    }

    private void createExplicitAPIIntent(String type, Uri bitmapUri, String packageName, String api){

        // Create the new Intent using the 'Send' action.
        Intent share = new Intent(Intent.ACTION_SEND);

        share.setPackage(packageName);
        // Set the MIME type
        share.setType(type);

        if (!IntentUtils.isIntentAvailable(activity, share)) {
            Toast.makeText(activity, api + " not found", Toast.LENGTH_LONG).show();
            shareAndroid(type, bitmapUri);
        }
        else {
            // Add the URI to the Intent.
            share.putExtra(Intent.EXTRA_STREAM, bitmapUri);

            // Broadcast the Intent.
            activity.startActivity(Intent.createChooser(share, "Share to"));
        }


    }
}
