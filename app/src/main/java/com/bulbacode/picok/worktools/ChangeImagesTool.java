package com.bulbacode.picok.worktools;

import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;

import com.bulbacode.picok.R;
import com.bulbacode.picok.WorkToolWithBgPorterSwitch;
import com.bulbacode.picok.WorkView;
import com.bulbacode.picok.activities.EditActivity;
import com.bulbacode.picok.utils.ImageCaptureUtils;
import com.bulbacode.picok.utils.ImageFileProperties;

/**
 * Created by Hadadi on 1/5/2016.
 *
 * Change images tool
 */
public class ChangeImagesTool extends WorkToolWithBgPorterSwitch {
    private final ImageButton pickFromGalleryButton;
    private final ImageButton takePictureButton;
    private final SeekBar opacityBar;
    private static ImageFileProperties imageFileProperties;

    public ChangeImagesTool() {
        super(R.id.change_images_tool_bg_porter_switch,
                R.layout.change_images_tool_layout,
                R.drawable.add,
                R.drawable.add_sel,
                R.id.change_images_tool_button);

        // We don't want the "inner" button to be active by default
        disableActivateInnerButtonOnDefault();

        // Hooking the gallery and camera buttons
        pickFromGalleryButton = (ImageButton) this.toolView.findViewById(R.id.change_image_gallery_button);
        pickFromGalleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPickFromGalleryClicked();
            }
        });

        takePictureButton = (ImageButton) this.toolView.findViewById(R.id.change_image_camera_button);
        takePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTakePictureClicked();
            }
        });

        opacityBar = (SeekBar) this.toolView.findViewById(R.id.opacity_bar);

        opacityBar.setProgress(100);

        opacityBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                WorkView.getInstance().setOpacity(progress / 100f);
            }
        });
    }

    public void onActivate() {
        super.onActivate();

        setIsOuterAltered(false);
        setIsInnerAltered(false);

        if (WorkView.getInstance().getIsInversed())
            switchToOuter();
        else
            switchToInner();

        opacityBar.setProgress(Math.round(WorkView.getInstance().getOpacity() * 100));
    }

    public void onSwitchToOuter() {
        super.onSwitchToOuter();

        WorkView.getInstance().changeInverseMask(true);
    }

    public void onSwitchToInner() {
        super.onSwitchToInner();

        WorkView.getInstance().changeInverseMask(false);
    }

    private void onPickFromGalleryClicked() {
        // TODO we added 1000 checks and at this place it's not
        pickGallery();


    }

    private void pickGallery() {
        imageFileProperties = ImageCaptureUtils.dispatchStartGalleryImagePicker(EditActivity.getActivity());
        imageFileProperties.isInner = this.isInner;
    }

    private void onTakePictureClicked() {
        // TODO we added 1000 checks and at this place it's not
        takePicture();


    }

    private void takePicture() {
        imageFileProperties = ImageCaptureUtils.dispatchCameraIntent(EditActivity.getActivity());
        imageFileProperties.isInner = this.isInner;
    }

    public ImageFileProperties getImageFileProperties() {
        return imageFileProperties;
    }
}
