package com.bulbacode.picok.worktools;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;

import com.bulbacode.picok.DiagonalButton;
import com.bulbacode.picok.R;
import com.bulbacode.picok.WorkView;
import com.bulbacode.picok.activities.EditActivity;
import com.bulbacode.picok.utils.FlurryManager;

import yuku.ambilwarna.AmbilWarnaDialog;

/**
 * Created by Hadadi on 1/5/2016.
 *
 * Color and texture tool
 */
public class ColorTextureTool extends WorkTool {
    private static final String TAG = ColorTextureTool.class.getSimpleName();
    private static final float DEFAULT_ALPHA_FACTOR = 0.5f;
    private static final int ON_START_DEFAULT_PROGRESS = 70;

    private Integer[] fullTextureIDS = { R.drawable.tex1, R.drawable.tex2, R.drawable.tex3, R.drawable.tex4, R.drawable.tex5, R.drawable.tex6, R.drawable.tex7, R.drawable.tex8, R.drawable.tex9,
                       R.drawable.tex10, R.drawable.tex11, R.drawable.tex13, R.drawable.tex14, R.drawable.tex15, R.drawable.tex16, R.drawable.tex17, R.drawable.tex18, R.drawable.tex19,
                       R.drawable.tex20, R.drawable.tex21,R.drawable.tex22,R.drawable.tex23,R.drawable.tex24,R.drawable.tex25,R.drawable.tex26,R.drawable.tex27,R.drawable.tex28, R.drawable.tex29,
                       R.drawable.tex30,R.drawable.tex31,R.drawable.tex32,R.drawable.tex33,R.drawable.tex34,R.drawable.tex35,R.drawable.tex36,R.drawable.tex37,R.drawable.tex38,R.drawable.tex39,
                       R.drawable.tex40,R.drawable.tex41,R.drawable.tex42,R.drawable.tex43,R.drawable.tex44,R.drawable.tex45,R.drawable.tex46,R.drawable.tex47,R.drawable.tex48,R.drawable.tex49,
                       R.drawable.tex50
    };

    // Widgets
    private final SeekBar opacityBar;

    private final DiagonalButton colorPickerButton;
    private final ImageButton colorInnerButton;
    private final ImageButton colorOuterButton;
    private final ImageButton colorInnerStatus;
    private final ImageButton colorOuterStatus;

    private final DiagonalButton texturePickerButton;
    private final ImageButton textureInnerButton;
    private final ImageButton textureOuterButton;
    private final ImageButton textureInnerStatus;
    private final ImageButton textureOuterStatus;

    private boolean isInnerColorAltered;
    private boolean isOuterColorAltered;
    private boolean isInnerTextureAltered;
    private boolean isOuterTextureAltered;

    private AlertDialog textureDialog;
    private GridView gridView;

    private boolean isColorInner = true; // default pick is always outer
    private boolean isTextureInner = true; // default pick is always outer
    private boolean isColorSelected;
    private boolean isTextureSelected;

    private final int screenWidth;
    private final int heightWidth;
    private int color;

    private int innerColor = 0;
    private int outerColor = 0;

    private int innerColorAlpha;
    private int outerColorAlpha;

    private int innerTextureAlpha;
    private int outerTextureAlpha;

    private Drawable chosenTexture;

    private Drawable innerTexture;
    private Drawable outerTexture;

    // texture constants
    private static final int DEFAULT_DIVIDER = 4;


    public ColorTextureTool() {
        super(R.layout.color_texture_tool_layout,
                R.drawable.color,
                R.drawable.color_sel,
                R.id.color_texture_button);

//        textureTool.switchToNone();

//        this.currentColorAlpha = (int) DEFAULT_ALPHA_FACTOR;

        // Hooking the inner/outer switch buttons
        Log.d(TAG, "changed");
        colorInnerButton = (ImageButton) this.toolView.findViewById(R.id.color_inner_outer).findViewById(R.id.porter_switch_button);
        colorInnerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onColorTextureSwitchPressed(true, true);
            }
        });
        colorOuterButton = (ImageButton) this.toolView.findViewById(R.id.color_inner_outer).findViewById(R.id.bg_switch_button);
        colorOuterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onColorTextureSwitchPressed(true, false);
            }
        });
        textureInnerButton = (ImageButton) this.toolView.findViewById(R.id.texture_inner_outer).findViewById(R.id.porter_switch_button);
        textureInnerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onColorTextureSwitchPressed(false, true);
            }
        });
        textureOuterButton = (ImageButton) this.toolView.findViewById(R.id.texture_inner_outer).findViewById(R.id.bg_switch_button);
        textureOuterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onColorTextureSwitchPressed(false, false);
            }
        });

        // Hooking the color and texture buttons
        colorPickerButton = (DiagonalButton) this.toolView.findViewById(R.id.color_picker_button);
        colorPickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startColorPicker();
                isColorSelected = true;
                isTextureSelected = false;
            }
        });

        texturePickerButton = (DiagonalButton) this.toolView.findViewById(R.id.texture_picker_button);
        texturePickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startTexturePicker();
                isColorSelected = false;
                isTextureSelected = true;
            }
        });

        colorInnerStatus = (ImageButton) toolView.findViewById(R.id.color_inner_outer).findViewById(R.id.in_status);
        colorOuterStatus = (ImageButton) toolView.findViewById(R.id.color_inner_outer).findViewById(R.id.out_status);

        textureInnerStatus = (ImageButton) toolView.findViewById(R.id.texture_inner_outer).findViewById(R.id.in_status);
        textureOuterStatus = (ImageButton) toolView.findViewById(R.id.texture_inner_outer).findViewById(R.id.out_status);


        opacityBar = (SeekBar) this.toolView.findViewById(R.id.color_texture_seek_bar);

        opacityBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                // a check to see if the seekbar was moved or the change was triggered by code
                onSeekBarProgressChange(progress, fromUser);
            }
        });

        DisplayMetrics displaymetrics = new DisplayMetrics();
        EditActivity.getActivity().getWindowManager().getDefaultDisplay()
                .getMetrics(displaymetrics);

        screenWidth = displaymetrics.widthPixels;
        heightWidth = displaymetrics.heightPixels;

        grid();

        // initializing all alphas
        innerColorAlpha = opacityBar.getProgress();
        outerColorAlpha = innerColorAlpha;
        innerTextureAlpha = innerColorAlpha;
        outerTextureAlpha = innerColorAlpha;

        // init
//        colorOuterButton.setColorFilter(Color.RED);
//        switchToNone(colorInnerButton, colorOuterButton, isInnerColorAltered, isOuterColorAltered, colorInnerStatus, colorOuterStatus);
//        switchToNone(textureInnerButton, textureOuterButton, isInnerTextureAltered, isOuterTextureAltered, textureInnerStatus, textureOuterStatus);
        switchToInner(colorInnerButton, colorOuterButton, colorInnerStatus, colorOuterStatus);
    }

    private void onSeekBarProgressChange(int progress, boolean fromUser) {
        if (fromUser) {
            float progressFloat = progress / 100f;

            if (isColorSelected) {
                // check if color has been chosen
                if (ColorTextureTool.this.color != 0) {
                    int chosenColor;
                    if (isColorInner) {
                        innerColorAlpha = progress;
                        chosenColor = ColorTextureTool.this.innerColor;
//                        colorPickerButton.setUpperColor(chosenColor);

                    }
                    else {
                        outerColorAlpha = progress;
                        chosenColor = ColorTextureTool.this.outerColor;
//                        colorPickerButton.setLowerColor(chosenColor);
                    }

                    WorkView.getInstance().setColor(chosenColor, ColorTextureTool.this.isColorInner, progressFloat);
//                        ColorTextureTool.this.currentColorAlpha = progress;

                }
            }
            else if (isTextureSelected){ // texture selected
                if (isTextureInner) {
                    innerTextureAlpha = progress;
                }
                else {
                    outerTextureAlpha = progress;
                }
                WorkView.getInstance().setColorTextureOpacity(progressFloat, isTextureInner);
            }
        }
    }

    private void grid() {

        AlertDialog.Builder builder;
//        TODO replace the inflator with context inflator
        final Activity activity = EditActivity.getActivity();
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.category_dialog,(ViewGroup) activity.findViewById(R.id.layout_root));

        builder = new AlertDialog.Builder(activity);
        builder.setView(layout);

        textureDialog = builder.create();

        gridView = (GridView)layout.findViewById(R.id.gridview);
        gridView.setAdapter(new ImageAdapter(activity));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                applyTexture(position);
            }
        });

        ImageButton closeButton = (ImageButton) layout.findViewById(R.id.x_button);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textureDialog.cancel();
            }
        });

    }

    private void applyTexture(int position) {

        Toast.makeText(EditActivity.getActivity(), "position:" + position, Toast.LENGTH_SHORT).show();

        int resId = fullTextureIDS[position];
        chosenTexture = com.wnafee.vector.compat.ResourcesCompat.getDrawable((Context) EditActivity.getActivity(), resId);
        Bitmap bitmap = ((BitmapDrawable) chosenTexture).getBitmap();

        int alpha;
        if (opacityBar.getProgress() != 0) {
            alpha = opacityBar.getProgress();
        } else {
            alpha = ON_START_DEFAULT_PROGRESS;
        }
        // mock a press, in order to color the button if first time
        onColorTextureSwitchPressed(false, isTextureInner);
        if (isTextureInner) {
            innerTexture = chosenTexture;
            texturePickerButton.setLowerTextureBitmap(bitmap);
            isInnerTextureAltered = true;
            innerTextureAlpha = alpha;
        }
        else {
            outerTexture = chosenTexture;
            texturePickerButton.setUpperTextureBitmap(bitmap);
            isOuterTextureAltered = true;
            outerTextureAlpha = alpha;
        }

        opacityBar.setProgress(alpha);
        WorkView.getInstance().setColorTextureOpacity(alpha / 100f, isTextureInner);

        WorkView.getInstance().setTexture(chosenTexture, isTextureInner);


        textureDialog.cancel();
    }

    private void startTexturePicker() {
        textureDialog.show();
    }


    private void onColorTextureSwitchPressed(boolean isColor, boolean isInner) {
        ImageButton curInnerButton;
        ImageButton curOuterButton;
        ImageButton curInnerStatusButton;
        ImageButton curOuterStatusButton;



        // TODO any chance to refactor this ?
        if (isColor) {
            FlurryManager.getInstance().featureEvent(WorkView.ToolFeatureEnum.COLOR);
            // Giving the selected color to inner/outer, and resetting the color of the other outer/inner
            this.isColorSelected = true;
            this.isTextureSelected = false;

            curInnerButton = this.colorInnerButton;
            curOuterButton = this.colorOuterButton;
            curInnerStatusButton = this.colorInnerStatus;
            curOuterStatusButton = this.colorOuterStatus;

            // clearing
            this.textureInnerButton.clearColorFilter();
            this.textureOuterButton.clearColorFilter();

            // setting the previous value of the progress bar
            if (isInner) {
                opacityBar.setProgress(innerColorAlpha);
            }
            else {
                opacityBar.setProgress(outerColorAlpha);
            }

            this.isColorInner = isInner;
        }
        else { // Texture
            FlurryManager.getInstance().featureEvent(WorkView.ToolFeatureEnum.TEXTURE);

            this.isColorSelected = false;
            this.isTextureSelected = true;

            curInnerButton = this.textureInnerButton;
            curOuterButton = this.textureOuterButton;
            curInnerStatusButton = this.textureInnerStatus;
            curOuterStatusButton = this.textureOuterStatus;

            // Giving the selected texture to inner/outer, and resetting the texture of the other outer/inner
//            WorkView.getInstance().setTexture(chosenTexture, isInner);
            if (isInner) {
                opacityBar.setProgress(innerTextureAlpha);
            }
            else {
                opacityBar.setProgress(outerTextureAlpha);
            }

            this.isTextureInner = isInner;
        }
        switchToNone(colorInnerButton, colorOuterButton, isInnerColorAltered, isOuterColorAltered, colorInnerStatus, colorOuterStatus);
        switchToNone(textureInnerButton, textureOuterButton, isInnerTextureAltered, isOuterTextureAltered, textureInnerStatus, textureOuterStatus);

        if (isInner) {
            switchToInner(curInnerButton, curOuterButton, curInnerStatusButton, curOuterStatusButton);
        }
        else {
            switchToOuter(curInnerButton, curOuterButton, curInnerStatusButton, curOuterStatusButton);
        }


    }

    private void startColorPicker() {

            AmbilWarnaDialog dialog = new AmbilWarnaDialog(EditActivity.getActivity(), color, false, new AmbilWarnaDialog.OnAmbilWarnaListener() {
                @Override
                public void onOk(AmbilWarnaDialog dialog, int color) {
                    applyColor(color);
                }

                @Override
                public void onCancel(AmbilWarnaDialog dialog) {

                }
            });
            dialog.show();
    }

    private void applyColor(int color) {
        int alpha;
        if (opacityBar.getProgress() != 0) {
            alpha = opacityBar.getProgress();
        } else {
            alpha = ON_START_DEFAULT_PROGRESS;
        }

        onColorTextureSwitchPressed(true, isColorInner);
        if (isColorInner) {
            innerColor = color;
            colorPickerButton.setLowerColor(color);
            isInnerColorAltered = true;
            innerColorAlpha = alpha;
        }
        else {
            outerColor = color;
            colorPickerButton.setUpperColor(color);
            isOuterColorAltered = true;
            outerColorAlpha = alpha;
        }
        ColorTextureTool.this.color = color;
        opacityBar.setProgress(alpha);

        // TODO is inner is the default ?
        WorkView.getInstance().setColor(color, isColorInner, alpha / 100f);
    }

    public void onActivate() {
        super.onActivate();

        updateTextureInnerOuterState(true);
        updateColorInnerOuterState(true);
    }

    private void updateColorInnerOuterState(boolean isInner) {

    }

    private void updateTextureInnerOuterState(boolean isInner) {

    }

    public class ImageAdapter extends BaseAdapter {

        private Context mContext;
        private LayoutInflater mInflater;
        private final int IMAGE_WIDTH = Math.round(screenWidth / DEFAULT_DIVIDER);

        public ImageAdapter(Context c) {
            mInflater = LayoutInflater.from(c);
            mContext = c;

        }

        public int getCount() {
            return mThumbIds.length;
        }

        public Object getItem(int position) {
            return null;
        }
        public long getItemId(int position) {
            return 0;
        }
        // create a new ImageView for each item referenced by the
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;

            if (convertView == null) {  // if it's not recycled,

                convertView = mInflater.inflate(R.layout.dialog_category_content, null);
                convertView.setLayoutParams(new GridView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                holder = new ViewHolder();
                holder.icon = (ImageView)convertView.findViewById(R.id.categoryimage);


                convertView.setTag(holder);
            }
            else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.icon.setAdjustViewBounds(true);
            holder.icon.setScaleType(ImageView.ScaleType.CENTER_CROP);
            holder.icon.setImageResource(mThumbIds[position]);

            return convertView;
        }
        class ViewHolder {
            ImageView icon;
        }

        // references to our images
        // no tex_s12
        private Integer[] mThumbIds = {
                R.drawable.tex_s1, R.drawable.tex_s2, R.drawable.tex_s3, R.drawable.tex_s4, R.drawable.tex_s5, R.drawable.tex_s6, R.drawable.tex_s7, R.drawable.tex_s8,
                R.drawable.tex_s9, R.drawable.tex_s10, R.drawable.tex_s11, R.drawable.tex_s13, R.drawable.tex_s14, R.drawable.tex_s15, R.drawable.tex_s16, R.drawable.tex_s17, R.drawable.tex_s18,
                R.drawable.tex_s19, R.drawable.tex_s20, R.drawable.tex_s21, R.drawable.tex_s22, R.drawable.tex_s23, R.drawable.tex_s24, R.drawable.tex_s25, R.drawable.tex_s26, R.drawable.tex_s27, R.drawable.tex_s28,
                R.drawable.tex_s29, R.drawable.tex_s30, R.drawable.tex_s31,R.drawable.tex_s32, R.drawable.tex_s33,R.drawable.tex_s34,R.drawable.tex_s35,R.drawable.tex_s36,R.drawable.tex_s37,R.drawable.tex_s38,
                R.drawable.tex_s39, R.drawable.tex_s40, R.drawable.tex_s41,R.drawable.tex_s42,R.drawable.tex_s43,R.drawable.tex_s44,R.drawable.tex_s45,R.drawable.tex_s46,R.drawable.tex_s47,R.drawable.tex_s48,R.drawable.tex_s49,
                R.drawable.tex_s50

        };

    }

    public void switchToNone(ImageButton innerButton, ImageButton outerButton, boolean isInnerAltered
    , boolean isOuterAltered, ImageButton innerStatus, ImageButton outerStatus) {
        this.isColorInner = true;
        this.isTextureInner = true;

        outerButton.setImageResource(R.drawable.out);
        innerButton.setImageResource(R.drawable.in);

        if (isInnerAltered) {
            innerStatus.setVisibility(View.VISIBLE);
            innerStatus.setImageResource(R.drawable.dot);
        }
        else {
            innerStatus.setVisibility(View.INVISIBLE);
        }

        if (isOuterAltered) {
            outerStatus.setVisibility(View.VISIBLE);
            outerStatus.setImageResource(R.drawable.dot);
        }
        else {
            outerStatus.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * Call this when you don't want the default "inner" button be active each time going to this tool
     */
//    protected void disableActivateInnerButtonOnDefault() {
//        this.activateInnerButtonOnDefault = false;
//    }

    protected void switchToInner(ImageButton innerButton, ImageButton outerButton,ImageButton innerStatus, ImageButton outerStatus) {
//        if (isInner)
//            return;
        if (isColorSelected) {
            isColorInner = true;
            setIsOuterAltered(isOuterColorAltered, outerStatus);

        }
        else {
            isTextureInner = true;
            setIsOuterAltered(isOuterTextureAltered, outerStatus);

        }

        innerButton.setImageResource(R.drawable.in_sel);
        outerButton.setImageResource(R.drawable.out);

        innerStatus.setVisibility(View.VISIBLE);
        innerStatus.setImageResource(R.drawable.sel_line);

    }


    protected void switchToOuter(ImageButton innerButton, ImageButton outerButton,ImageButton innerStatus, ImageButton outerStatus) {

        if (isColorSelected) {
            isColorInner = false;
            setIsInnerAltered(isInnerColorAltered, innerStatus);

        }
        else {
            isTextureInner = false;
            setIsInnerAltered(isInnerTextureAltered, innerStatus);

        }

        outerButton.setImageResource(R.drawable.out_sel);
        innerButton.setImageResource(R.drawable.in);

        outerStatus.setVisibility(View.VISIBLE);
        outerStatus.setImageResource(R.drawable.sel_line);
    }


    public void setIsInnerAltered(boolean isInnerAltered, ImageButton innerStatus) {
//        this.isInnerAltered = isInnerAltered;

        if (isInnerAltered) {
            innerStatus.setVisibility(View.VISIBLE);
            innerStatus.setImageResource(R.drawable.dot);
        }
        else {
            innerStatus.setVisibility(View.INVISIBLE);
        }
    }

    public void setIsOuterAltered(boolean isOuterAltered, ImageButton outerStatus) {
//        this.isOuterAltered = isOuterAltered;

        if (isOuterAltered) {
            outerStatus.setVisibility(View.VISIBLE);
            outerStatus.setImageResource(R.drawable.dot);
        }
        else {
            outerStatus.setVisibility(View.INVISIBLE);
        }
    }



}
