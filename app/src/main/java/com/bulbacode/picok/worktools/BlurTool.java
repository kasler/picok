package com.bulbacode.picok.worktools;

import com.bulbacode.picok.PicokScale;
import com.bulbacode.picok.R;
import com.bulbacode.picok.WorkToolWithBgPorterSwitch;
import com.bulbacode.picok.WorkView;

import android.widget.ImageButton;
import android.widget.ImageView;

/**
 * Created by Hadadi on 1/5/2016.
 *
 * Blur tool
 */
public class BlurTool extends WorkToolWithBgPorterSwitch {
    private final PicokScale scale;

    public BlurTool() {
        super(R.id.blur_tool_bg_porter_switch,
                R.layout.blur_tool_layout,
                R.drawable.blur,
                R.drawable.blur_sel,
                R.id.blur_tool_button);

        this.scale = new PicokScale(5,
                                    (ImageButton) this.toolView.findViewById(R.id.big_bullet),
                                        this.toolView.findViewById(R.id.blur_scale)) {
            @Override
            public void onScaled(int oldScale, int newScale) {
                WorkView.getInstance().setBlur(newScale, isInner);
            }
        };

        this.scale.addDefault((ImageButton) this.toolView.findViewById(R.id.blur_scale_btn_0),
                (ImageView) this.toolView.findViewById(R.id.blur_scale_line_0),
                true);

        this.scale.add((ImageButton) this.toolView.findViewById(R.id.blur_scale_btn_1),
                (ImageView) this.toolView.findViewById(R.id.blur_scale_line_1),
                true);

        this.scale.add((ImageButton) this.toolView.findViewById(R.id.blur_scale_btn_2),
                (ImageView) this.toolView.findViewById(R.id.blur_scale_line_2),
                true);

        this.scale.add((ImageButton) this.toolView.findViewById(R.id.blur_scale_btn_3),
                (ImageView) this.toolView.findViewById(R.id.blur_scale_line_3),
                true);

        this.scale.add((ImageButton) this.toolView.findViewById(R.id.blur_scale_btn_4),
                null,
                true);
    }

    protected void onSwitchToInner() {
        super.onSwitchToInner();

        // Setting the active blur button to the porter image's current blur
        scale.setActiveIndex(WorkView.getInstance().getBlur(true));

        setIsOuterAltered(WorkView.getInstance().getBlur(false) != 0);
    }

    protected void onSwitchToOuter() {
        super.onSwitchToOuter();

        // Setting the active blur button to the bg image's current blur
        scale.setActiveIndex(WorkView.getInstance().getBlur(false));

        setIsInnerAltered(WorkView.getInstance().getBlur(true) != 0);
    }
}
