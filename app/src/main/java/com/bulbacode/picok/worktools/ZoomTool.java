package com.bulbacode.picok.worktools;

import android.widget.ImageButton;
import android.widget.ImageView;

import com.bulbacode.picok.PicokScale;
import com.bulbacode.picok.R;
import com.bulbacode.picok.WorkToolWithBgPorterSwitch;
import com.bulbacode.picok.WorkView;
import com.bulbacode.picok.utils.ShapeMaskRes;

/**
 * Created by Hadadi on 1/5/2016.
 *
 * Zoom tool
 */
public class ZoomTool extends WorkToolWithBgPorterSwitch {
    private final PicokScale scale;

    public ZoomTool() {
        super(R.id.zoom_tool_bg_porter_switch,
                R.layout.zoom_tool_layout,
                R.drawable.zoom,
                R.drawable.zoom_sel,
                R.id.zoom_tool_button);

        this.scale = new PicokScale(5,
                                    (ImageButton) this.toolView.findViewById(R.id.big_bullet),
                                        this.toolView.findViewById(R.id.zoom_scale)) {
            @Override
            public void onScaled(int oldScale, int newScale) {
                WorkView.getInstance().setZoom(newScale, isInner);

                if (isInner)
                    checkZoomMaskSizesValidity();
            }
        };

        this.scale.add((ImageButton) this.toolView.findViewById(R.id.zoom_scale_btn_0),
                            (ImageView) this.toolView.findViewById(R.id.zoom_scale_line_0),
                                true);

        this.scale.add((ImageButton) this.toolView.findViewById(R.id.zoom_scale_btn_1),
                            (ImageView) this.toolView.findViewById(R.id.zoom_scale_line_1),
                                true);

        this.scale.addDefault((ImageButton) this.toolView.findViewById(R.id.zoom_scale_btn_2),
                                (ImageView) this.toolView.findViewById(R.id.zoom_scale_line_2),
                                    true);

        this.scale.add((ImageButton) this.toolView.findViewById(R.id.zoom_scale_btn_3),
                            (ImageView) this.toolView.findViewById(R.id.zoom_scale_line_3),
                                true);

        this.scale.add((ImageButton) this.toolView.findViewById(R.id.zoom_scale_btn_4),
                            null,
                            true);
    }

    private void checkZoomMaskSizesValidity() {
        if (ShapeMaskRes.getIsPreview())
            return;

        if (!WorkView.getInstance().getIsMultiplied() && WorkView.getInstance().getMaskSize() - WorkView.getInstance().getZoom(true) > 1)
            WorkView.getInstance().setMaskSize(WorkView.getInstance().getZoom(true) + 1);
    }

    protected void onSwitchToInner() {
        super.onSwitchToInner();

        setIsOuterAltered(WorkView.getInstance().getZoom(false) != 2);

        // Setting the active zoomMultiplier button to the porter image's current zoomMultiplier
        scale.setActiveIndex(WorkView.getInstance().getZoom(true));

        // Inner image can have any zoomMultiplier
        scale.enableAll();

        // int maskSize = WorkView.getInstance().getMaskSize();

        if (WorkView.getInstance().getIsMultiplied()) {
            if (WorkView.getInstance().getZoom(true) < 2) {
                WorkView.getInstance().setZoom(2, true);
            }

            scale.setIsEnabled(0, false);
            scale.setIsEnabled(1, false);
        }
    }

    protected void onSwitchToOuter() {
        super.onSwitchToOuter();

        setIsInnerAltered(WorkView.getInstance().getZoom(true) != 2);

        // Setting the active zoomMultiplier button to the bg image's current zoomMultiplier
        scale.setActiveIndex(WorkView.getInstance().getZoom(false));

        // Outer image can't zoomMultiplier-out
        scale.setIsEnabled(0, false);
        scale.setIsEnabled(1, false);
    }
}
