package com.bulbacode.picok.worktools;

import android.util.Log;
import android.widget.FrameLayout;

import com.bulbacode.picok.R;
import com.bulbacode.picok.activities.EditActivity;

/**
 * Created by Hadadi on 1/5/2016.
 *
 * Holds all work tools, knows what to do when switching between them
 */
public class WorkTools {
    private static final String TAG = WorkTool.class.getSimpleName();
    private static WorkTools ourInstance = null;
    private final ChangeImagesTool changeImageTool;

    private WorkTool curWorkTool;

    private FrameLayout toolFrame;

    public static synchronized WorkTools getInstance() {
        if (ourInstance == null)
            ourInstance = new WorkTools();

        return ourInstance;
    }

    public static synchronized void delete() {
        ourInstance = null;
    }

    private WorkTools() {
        this.toolFrame = (FrameLayout) EditActivity.getActivity().findViewById(R.id.tool_frame);

        // The default tool is the shape mask choosing tool
        this.curWorkTool = new ShapeTool();

        // Initializing the other tools, no need to save them,
        // they will do their job when their button will be clicked
        new ResizeTool();
        changeImageTool = new ChangeImagesTool();
        new ReflectTool();
        new ZoomTool();
        new BlurTool();
        new ColorTextureTool();
//        new ShareWorkTool();


        /* this.fragmentTabHost = fragmentTabHost;
        this.fragmentTabHost.setup(activity, fragmentManager, R.id.realtabcontent);
        this.fragmentTabHost.addTab(this.fragmentTabHost.newTabSpec("tab1").setIndicator("Tab1"),
                ShapeTool.class, null);

        this.fragmentTabHost.addTab(this.fragmentTabHost.newTabSpec("tab2").setIndicator("Tab2"),
                ResizeTool.class, null);
        this.fragmentTabHost.addTab(this.fragmentTabHost.newTabSpec("tab3").setIndicator("Tab3"),
                ChangeImagesTool.class, null);

        // Activating this first default tool
        this.curWorkTool.onActivate();

        return this.fragmentTabHost; */
    }

    public void init() {
        // Activating this first default tool
        switchToolLayout(this.curWorkTool);
    }

    public void switchToolLayout(WorkTool newWorkTool) {
        if (newWorkTool == null) {
            Log.w(TAG, "Switching to null work tool");
            return;
        }

        this.toolFrame.removeAllViews();

        // Deactivate the current one and onActivate the new one
        this.curWorkTool.deactivate();

        this.toolFrame.addView(newWorkTool.getMainView());
        newWorkTool.activate();

        this.curWorkTool = newWorkTool;
    }

    /**
     * Returns the frame layout in which we show each tool
     */
    public FrameLayout getToolFrame() {
        return toolFrame;
    }

    public ChangeImagesTool getChangeImageTool() {
        return changeImageTool;
    }
}
