package com.bulbacode.picok.worktools;

import com.bulbacode.picok.R;
import com.bulbacode.picok.WorkToolWithBgPorterSwitch;
import com.bulbacode.picok.WorkView;

import android.view.View;
import android.widget.ImageButton;

/**
 * Created by Hadadi on 1/5/2016.
 *
 * Reflect tool
 */
public class ReflectTool extends WorkToolWithBgPorterSwitch {
    private final ImageButton mirrorButton;
    private final ImageButton flipButton;

    private final ImageButton mirrorStatus;
    private final ImageButton flipStatus;

    public ReflectTool() {
        super(R.id.reflect_tool_bg_porter_switch,
                R.layout.reflect_tool_layout,
                R.drawable.reflect,
                R.drawable.reflect_sel,
                R.id.reflect_tool_button);

        // Hooking the mirror and flip buttons
        this.mirrorButton = (ImageButton) this.toolView.findViewById(R.id.mirror_button);
        this.mirrorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onMirrorClicked();
            }
        });

        this.flipButton = (ImageButton) this.toolView.findViewById(R.id.reflect_button);
        this.flipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFlipClicked();
            }
        });

        this.mirrorStatus = (ImageButton) this.toolView.findViewById(R.id.mirror_status);
        this.flipStatus = (ImageButton) this.toolView.findViewById(R.id.flip_status);
    }

    public void onSwitchToOuter() {
        super.onSwitchToOuter();

        setIsInnerAltered(WorkView.getInstance().getIsFlipped(true) ||
                            WorkView.getInstance().getIsMirrored(true));

        updateButtonsState(false);
    }

    public void onSwitchToInner() {
        super.onSwitchToInner();

        setIsOuterAltered(WorkView.getInstance().getIsFlipped(false) ||
                WorkView.getInstance().getIsMirrored(false));

        updateButtonsState(true);
    }

    private void updateButtonsState(boolean isInner) {
        if (WorkView.getInstance().getIsMirrored(isInner)) {
            this.mirrorButton.setImageResource(R.drawable.hor_sel);
            this.mirrorStatus.setVisibility(View.VISIBLE);
        }
        else {
            this.mirrorButton.setImageResource(R.drawable.hor);
            this.mirrorStatus.setVisibility(View.INVISIBLE);
        }

        if (WorkView.getInstance().getIsFlipped(isInner)) {
            this.flipButton.setImageResource(R.drawable.ver_sel);
            this.flipStatus.setVisibility(View.VISIBLE);
        }
        else {
            this.flipButton.setImageResource(R.drawable.ver);
            this.flipStatus.setVisibility(View.INVISIBLE);
        }
    }

    private void onMirrorClicked() {
        WorkView.getInstance().mirror(this.isInner);
        updateButtonsState(this.isInner);
    }

    private void onFlipClicked() {
        WorkView.getInstance().flip(this.isInner);
        updateButtonsState(this.isInner);
    }
}
