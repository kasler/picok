package com.bulbacode.picok.worktools;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bulbacode.picok.PicokScale;
import com.bulbacode.picok.R;
import com.bulbacode.picok.WorkView;
import com.bulbacode.picok.utils.ShapeMaskRes;

/**
 * Created by Hadadi on 1/5/2016.
 *
 * Resize tool
 */
public class ResizeTool extends WorkTool {

    private static final int DEFAULT_SIZE = 1;
    private final PicokScale scale;
    private final ImageView multiplyButton;

    public ResizeTool() {
        super(R.layout.resize_tool_layout,
                R.drawable.size,
                R.drawable.size_sel,
                R.id.resize_tool_button);

        this.multiplyButton = (ImageButton) this.toolView.findViewById(R.id.multiply_button);
        this.multiplyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onMultiplyPressed();
            }
        });

        this.scale = new PicokScale(4,
                                    (ImageButton) this.toolView.findViewById(R.id.big_bullet),
                                        this.toolView.findViewById(R.id.resize_scale)) {
            @Override
            public void onScaled(int oldScale, int newScale) {
                WorkView.getInstance().setMaskSize(newScale);

                checkMaskZoomSizesValidity();

                updateMultiplyButtonState();
            }
        };

        this.scale.add((ImageButton) this.toolView.findViewById(R.id.resize_scale_btn_0),
                (ImageView) this.toolView.findViewById(R.id.resize_scale_line_0),
                true);

        this.scale.add((ImageButton) this.toolView.findViewById(R.id.resize_scale_btn_1),
                (ImageView) this.toolView.findViewById(R.id.resize_scale_line_1),
                true);

        this.scale.add((ImageButton) this.toolView.findViewById(R.id.resize_scale_btn_2),
                (ImageView) this.toolView.findViewById(R.id.resize_scale_line_2),
                true);

        this.scale.add((ImageButton) this.toolView.findViewById(R.id.resize_scale_btn_3),
                null,
                true);

        this.scale.setActiveIndex(DEFAULT_SIZE);
    }

    public void onActivate() {
        super.onActivate();

        this.scale.setActiveIndex(WorkView.getInstance().getMaskSize());

        checkMaskZoomSizesValidity();

        updateMultiplyButtonState();
    }

    private void onMultiplyPressed() {
        WorkView.getInstance().switchMultiply();

        ResizeTool.checkMaskZoomSizesValidity();

        updateMultiplyButtonState();
    }

    public static void checkMaskZoomSizesValidity() {
        if (ShapeMaskRes.getIsPreview())
            return;

        if (WorkView.getInstance().getIsMultiplied() && WorkView.getInstance().getZoom(true) < 2)
            WorkView.getInstance().setZoom(2, true);
        else if (!WorkView.getInstance().getIsMultiplied() && WorkView.getInstance().getMaskSize() - WorkView.getInstance().getZoom(true) > 1)
            WorkView.getInstance().setZoom(WorkView.getInstance().getMaskSize() - 1, true);
    }

    private void updateMultiplyButtonState() {
        if (WorkView.getInstance().getMaskSize() == 3)
            this.multiplyButton.setImageResource(R.drawable.multi_dis);
        else if (WorkView.getInstance().getIsMultiplied())
            this.multiplyButton.setImageResource(R.drawable.multi_sel);
        else
            this.multiplyButton.setImageResource(R.drawable.multi);
    }
}
