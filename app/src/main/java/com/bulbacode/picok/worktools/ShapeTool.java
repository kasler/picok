package com.bulbacode.picok.worktools;

import android.animation.ObjectAnimator;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bulbacode.picok.R;
import com.bulbacode.picok.ShapeList;
import com.bulbacode.picok.activities.EditActivity;
import com.bulbacode.picok.utils.FlurryManager;

/**
 * Created by Hadadi on 1/5/2016.
 *
 * Shape mask chooser tool
 */
public class ShapeTool extends WorkTool {
    private static final long ANIMATION_SPEED = 300;
    private final View shapeFamiliesFrame;

    public enum ShapeFamilyCode {
        CIRCLES(0), SQUARES(1), TRIANGLES(2), STARS(3);
        public int value;

        ShapeFamilyCode(int value) {
            this.value = value;
        }
    }

    private final ImageButton backToFamiliesButton;
    private ShapeList shapeList;
    private RecyclerView shapesView;

    private int backToFamiliesButtonWidth;
    private float shapeFamiliesFrameHeight;
    private int shapesViewInitialX;

    private boolean isInsideShapeFamily;

    public ShapeTool() {
        super(R.layout.shape_tool_layout,
                R.drawable.add,
                R.drawable.add_sel,
                R.id.shape_tool_button);

        this.shapesView = (RecyclerView) this.toolView.findViewById(R.id.shapeList);
        this.shapeList = new ShapeList(shapesView);

        shapesView.setX(EditActivity.screenWidth);

        registerFamily(R.id.circles, ShapeFamilyCode.CIRCLES,
                R.drawable.cir);

        registerFamily(R.id.squares, ShapeFamilyCode.SQUARES,
                R.drawable.rec);

        registerFamily(R.id.triangles, ShapeFamilyCode.TRIANGLES,
                R.drawable.tri);

        registerFamily(R.id.stars, ShapeFamilyCode.STARS,
                R.drawable.star, true);

        this.backToFamiliesButton = (ImageButton) this.toolView.findViewById(R.id.shape_back_button);
        this.backToFamiliesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackToFamiliesPressed();
            }
        });

        // Initializing back button's location (only when the view observer learns it's width, to put it offscreen)
        ViewTreeObserver vto = this.backToFamiliesButton.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (backToFamiliesButtonWidth != 0)
                    return;

                backToFamiliesButtonWidth = backToFamiliesButton.getWidth();

                if (isInsideShapeFamily)
                    backToFamiliesButton.setX(0);
                else
                    backToFamiliesButton.setX(-backToFamiliesButton.getWidth());

                // Good time to understand where the shapes view is for it's animation (right after the back button)
                shapesViewInitialX = backToFamiliesButtonWidth;
            }
        });

        this.shapeFamiliesFrame = this.toolView.findViewById(R.id.shape_families);

        vto = this.shapeFamiliesFrame.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (shapeFamiliesFrameHeight != 0)
                    return;

                shapeFamiliesFrameHeight = shapeFamiliesFrame.getHeight();

                if (isInsideShapeFamily)
                    shapeFamiliesFrame.setY(-shapeFamiliesFrameHeight);
                else
                    shapeFamiliesFrame.setY(0);
            }
        });

        this.shapesView.setX(EditActivity.screenWidth);
        this.shapesView.setScaleY(0.2f);
    }

    public void onResume() {
        // We should refresh the views

    }

    private void onBackToFamiliesPressed() {
        ObjectAnimator anim = ObjectAnimator.ofFloat(backToFamiliesButton, "x", -backToFamiliesButton.getMeasuredWidth());
        anim.setDuration(ANIMATION_SPEED);
        anim.start();

        anim = ObjectAnimator.ofFloat(shapeFamiliesFrame, "y", 0);
        anim.setDuration(ANIMATION_SPEED);
        anim.start();

        anim = ObjectAnimator.ofFloat(shapesView, "x", EditActivity.screenWidth);
        anim.setDuration(ANIMATION_SPEED);
        anim.start();
        anim = ObjectAnimator.ofFloat(shapesView, "scaleY", 0.2f);
        anim.setDuration(ANIMATION_SPEED - 50);
        anim.start();

        isInsideShapeFamily = false;
    }

    private void registerFamily(int viewRes, final ShapeFamilyCode family,
                                int inactiveRes) {
        registerFamily(viewRes, family, inactiveRes, false);
    }

    private void registerFamily(int viewRes, final ShapeFamilyCode family,
                                    int inactiveRes, boolean hideSeperator) {
        ShapeFamily shapeFamily = new ShapeFamily(viewRes, family, inactiveRes);

        if (hideSeperator)
            shapeFamily.hideSeperator();
    }

    private void onFamilyClicked() {
        ObjectAnimator inAnim = ObjectAnimator.ofFloat(shapesView, "x", shapesViewInitialX);
        inAnim.setDuration(ANIMATION_SPEED);
        inAnim.start();
        inAnim = ObjectAnimator.ofFloat(shapesView, "scaleY", 1);
        inAnim.setDuration(ANIMATION_SPEED - 50);
        inAnim.start();

        inAnim = ObjectAnimator.ofFloat(shapeFamiliesFrame, "y", -shapeFamiliesFrameHeight);
        inAnim.setDuration(ANIMATION_SPEED);
        inAnim.start();

        inAnim = ObjectAnimator.ofFloat(backToFamiliesButton, "x", 0);
        inAnim.setDuration(ANIMATION_SPEED);
        inAnim.start();

        this.isInsideShapeFamily = true;
    }

    public static ShapeFamily selectedShapeFamily = null;

    private class ShapeFamily {
        private final View familyView;
        private final ShapeFamilyCode familyCode;
        private final int inactiveRes;

        public ShapeFamily(int viewRes, final ShapeFamilyCode familyCode,
                            int inactiveRes) {
            this.familyCode = familyCode;
            this.inactiveRes = inactiveRes;
            this.familyView = toolView.findViewById(viewRes);

            this.familyView.findViewById(R.id.shape_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClicked();
                }
            });

            ((ImageView) familyView.findViewById(R.id.shape_rotate_button)).
                                        setImageResource(R.drawable.shape_drop);


            ((ImageButton) this.familyView.findViewById(R.id.shape_button)).setImageResource(this.inactiveRes);

            // If none is selected yet, it's us - the first ones
            if (selectedShapeFamily == null)
                this.select();
            // Else we are deselected
            else
                this.deselect();
        }

        private void onClicked() {
            select();
            onFamilyClicked();
            shapeList.loadShapes(familyCode);
        }

        private void select() {

            if (selectedShapeFamily != null &&
                    familyCode == selectedShapeFamily.familyCode) {
                return;
            }

            // Applying select underline, and shape color
            this.familyView.findViewById(R.id.shape_selected).setVisibility(View.VISIBLE);
            ((ImageButton) this.familyView.findViewById(R.id.shape_button)).setColorFilter(ContextCompat.getColor(EditActivity.getActivity().getApplicationContext(), R.color.picok_pink), PorterDuff.Mode.SRC_IN);

            // Deselecting previous selection
            if (selectedShapeFamily != null)
                selectedShapeFamily.deselect();

            // Saving ourselves as selected
            selectedShapeFamily = this;

            FlurryManager flurryManager = FlurryManager.getInstance();
            flurryManager.baseShapeEvent(selectedShapeFamily.familyCode);
        }

        private void deselect() {
            // Applying select underline, and shape color
            this.familyView.findViewById(R.id.shape_selected).setVisibility(View.INVISIBLE);
            ((ImageButton) this.familyView.findViewById(R.id.shape_button)).clearColorFilter();
        }

        public void hideSeperator() {
            this.familyView.findViewById(R.id.shape_seperator).setVisibility(View.INVISIBLE);
        }
    }
}
