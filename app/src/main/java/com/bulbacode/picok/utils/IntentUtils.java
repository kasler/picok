package com.bulbacode.picok.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import java.util.List;

/**
 * Created by sergey on 2/3/16.
 */
public class IntentUtils {

    public static final String FACEBOOK_PACKAGE_NAME = "com.facebook.katana";
    public static final String INSTAGRAM_PACKAGE_NAME = "com.instagram.android";

    public static boolean isIntentAvailable(Context ctx, Intent intent) {
        final PackageManager packageManager = ctx.getPackageManager();
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }
}
