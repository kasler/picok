package com.bulbacode.picok.utils;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Created by sergey on 7/17/16.
 */
public class PermissionManager {

    public static final int REQUEST_WRITE_STORAGE_CAMERA = 70;
    public static final int REQUEST_WRITE_STORAGE_GALLERY = 71;
    public static final int REQUEST_WRITE_STORAGE_GENERAL = 72;


    public static void checkWritePermission(Activity activity) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (!isReadStorageAllowed(activity)) {
                requestStoragePermission(activity, REQUEST_WRITE_STORAGE_GENERAL);
            }
        }

    }

    public static void requestStoragePermission(Activity activity, int REQUEST){

        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                //If the user has denied the permission previously your code will come to this block
                //Here you can explain why you need this permission
                //Explain here why you need this permission
            }

            //And finally ask for the permission
            ActivityCompat.requestPermissions(activity, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},REQUEST);
        }

    }

    public static boolean isReadStorageAllowed(Activity activity) {
        //Getting the permission status
        if (Build.VERSION.SDK_INT >= 23) {
            int result = ContextCompat.checkSelfPermission(activity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

            //If permission is granted returning true
            if (result == PackageManager.PERMISSION_GRANTED)
                return true;

            //If permission is not granted returning false
            return false;
        }
        return true;
    }

//    private ArrayList findUnAskedPermissions(Activity activity, ArrayList<String> wanted){
//
//        ArrayList<String> result = new ArrayList<>();
//
//        for(String perm : wanted){
//
//            if(!hasPermission(activity, perm)){
//
//                result.add(perm);
//            }
//        }
//
//        return result;
//    }
//
//    @TargetApi(Build.VERSION_CODES.M)
//    private boolean hasPermission(Activity activity, String permission){
//        return(activity.checkSelfPermission(permission)==PackageManager.PERMISSION_GRANTED);
//
//    }

}
