package com.bulbacode.picok.utils;

import java.io.File;

/**
 * A simple POJO class
 */
public class ImageFileProperties {
    public File file;
    public File compressedFile;
    public String path;
    public String compressedPath;

    // important, this variable is set later by ChaneImageTool
    public boolean isInner;

    public ImageFileProperties(File file, String path, String compressedPath, File imageCompressed) {
        this.file = file;
        this.path = path;
        this.compressedPath = compressedPath;
        this.compressedFile = imageCompressed;
    }


}
