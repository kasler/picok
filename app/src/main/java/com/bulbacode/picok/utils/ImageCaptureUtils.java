package com.bulbacode.picok.utils;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.view.View;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A utility class for all image/bitmap actions
 */
public class ImageCaptureUtils {

    private static final String TAG = ImageCaptureUtils.class.getSimpleName();

    public static final int PICK_IMAGE_REQUEST = 1;
    public static final int CAMERA_IMAGE = 2;
    public static final int DEFAULT_QUALITY = 70;
    private static Uri latestUri;
    private static ContentResolver latestContentResolver;

    // gets an image from a variety of mobile folders
    public static ImageFileProperties dispatchStartGalleryImagePicker(Activity activity) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");

        activity.startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
        return createImageFileAndPhotoPath(false, true);
    }

    // opening a camera and savin
    public static ImageFileProperties dispatchCameraIntent(Activity activity) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        ImageFileProperties imageFileProperties = null;
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {

            imageFileProperties = createImageFileAndPhotoPath(true, true);
            File photoFile = imageFileProperties.file;
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                activity.startActivityForResult(takePictureIntent, CAMERA_IMAGE);
            }

        }
        return imageFileProperties;
    }



    public static ImageFileProperties createImageFileAndPhotoPath(boolean isCamera, boolean isCompress) {
        // Create an thumbnail file name
        ImageFileProperties imageFileProperties = null;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "Picok_image_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);

        // Create the compressed cache file
        File image = null;
        File imageCompressed = null;
        String path = "";
        String compressedPath = "";
        try {
            // gallery photos already have a regular path
            if (isCamera) {
                image = File.createTempFile(
                        imageFileName,  /* prefix */
                        ".jpg",         /* suffix */
                        storageDir//c.getCacheDir()      /* directory */
                );
                path = image.getAbsolutePath();
                LogUtils.printString(TAG, path, "photoPath");
            }

            if (isCompress) {
                imageCompressed = File.createTempFile(
                        imageFileName + "compressed_",  /* prefix */
                        ".jpg",         /* suffix */
                        // TODO Using cacheDir gives problems - the compressed phero thumbnail remains large
                        storageDir//c.getCacheDir()      /* directory */
                );

                compressedPath = imageCompressed.getAbsolutePath();
            }


            LogUtils.printString(TAG, path, "photoPath");

            imageFileProperties = new ImageFileProperties(image, path, compressedPath, imageCompressed);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return imageFileProperties;
    }

    public static void galleryAddPic(Activity activity, String currentPhotoPath) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(currentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        activity.sendBroadcast(mediaScanIntent);
    }

    public static String getUriFromGalleryImage(Intent data, ContentResolver contentResolver) {
        String path = "";
        Uri selectedImage = data.getData();

        latestUri = selectedImage;
        latestContentResolver = contentResolver;

        if (selectedImage != null ) {

            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            // Getting the thumbnail path
            Cursor cursor = contentResolver.query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndexOrThrow(filePathColumn[0]);
            path = cursor.getString(columnIndex);
            cursor.close();

            if (path == null || path.isEmpty()) {
                // Fallback
                path = selectedImage.getPath();
            }
        }

        return path;
    }

    public static Uri getUriFromCameraImage(String currentPhotoPath) {
        return Uri.fromFile(new File(currentPhotoPath));
    }

    public static void writeBitmapToFile(Bitmap bm, File file, int quality, Bitmap.CompressFormat compressFormat) throws IOException {
        FileOutputStream fos = new FileOutputStream(file);
        bm.compress(compressFormat, quality, fos);
        fos.flush();
        fos.close();
    }

    public static Bitmap getBitmapFromURI(Uri uri, ContentResolver contentResolver) {
        Bitmap bitmap = null;
        try {
            String filePath = uri.getPath();
            bitmap = BitmapFactory.decodeFile(filePath);
        }
        catch (Exception e) {
            ParcelFileDescriptor parcelFileDescriptor;
            try {
                parcelFileDescriptor = contentResolver.openFileDescriptor(uri, "r");
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
                return null;
            }

            FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
            return BitmapFactory.decodeFileDescriptor(fileDescriptor);
        }
        catch (OutOfMemoryError e) {
            e.printStackTrace();
        }


        return bitmap;
    }

    public static Bitmap getViewBitmap(View layout) {
        layout.buildDrawingCache();
        Bitmap b1 = layout.getDrawingCache();
        // copy this bitmap otherwise destroying the cache will destroy
        // the bitmap for the referencing drawable and you'll not
        // get the captured view

        Bitmap b = b1.copy(Bitmap.Config.ARGB_8888, false);
        layout.destroyDrawingCache();
        return b;
    }

    public static Uri getUriFromBitmap(Bitmap bitmap, int imageQuality, Bitmap.CompressFormat compressFormat) {
        File file = null;
        Uri uri = null;
        try {
            ImageFileProperties imageFileProperties = ImageCaptureUtils.createImageFileAndPhotoPath(true, true);
            file = imageFileProperties.file;
            ImageCaptureUtils.writeBitmapToFile(bitmap, file, imageQuality, compressFormat);
            uri = ImageCaptureUtils.getUriFromCameraImage(imageFileProperties.path);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return uri;
    }

    public static void decodeBitmapFromFile(int reqWidth, int reqHeight, String path, String compressedPath, int quality, Bitmap.CompressFormat compressFormat) {
        Bitmap compressedBitmap = settingCompression(reqWidth, reqHeight, path);
        FileOutputStream fOut;

        try {
            File f2 = new File(compressedPath);
            fOut = new FileOutputStream(f2);
            compressedBitmap.compress(compressFormat, quality, fOut);
            fOut.flush();
            fOut.close();
            compressedBitmap.recycle();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Bitmap settingCompression(int reqWidth, int reqHeight, String path) {
        int height;
        int width;

        // First decode with inJustDecodeBounds=true to check dimensions
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        width = options.outWidth / options.inSampleSize;
        height = options.outHeight / options.inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        Bitmap bitmap =  BitmapFactory.decodeFile(path, options);

        if (bitmap == null) {
            ParcelFileDescriptor parcelFileDescriptor;
            try {
                parcelFileDescriptor = latestContentResolver.openFileDescriptor(latestUri, "r");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return null;
            }

            FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();

            options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;

            BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);

            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;

            return BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
        }
        else {
            ExifInterface exif;
            try {
                exif = new ExifInterface(path);
            } catch (IOException e) {
                e.printStackTrace();
                return bitmap;
            }

            String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
            int orientation = orientString != null ? Integer.parseInt(orientString) : ExifInterface.ORIENTATION_NORMAL;

            int rotationAngle = 0;
            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90;
            if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180;
            if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270;

            Matrix matrix = new Matrix();
            matrix.setRotate(rotationAngle, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);

            return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        }
    }

    private static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of thumbnail
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }


    // from http://stackoverflow.com/a/9194259/1824521
    // Turns the bitmap to a mutable object (sometimes a bitmap is immutable
    // and we can't change it)
    public static Bitmap convertToMutable(Bitmap imgIn) {
        try {
            //this is the file going to use temporally to save the bytes.
            // This file will not be a image, it will store the raw image data.
            File file = new File(Environment.getExternalStorageDirectory() + File.separator + "temp.tmp");

            //Open an RandomAccessFile
            //Make sure you have added uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"
            //into AndroidManifest.xml file
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");

            // get the width and height of the source bitmap.
            int width = imgIn.getWidth();
            int height = imgIn.getHeight();
            Bitmap.Config type = imgIn.getConfig();

            //Copy the byte to the file
            //Assume source bitmap loaded using options.inPreferredConfig = Config.ARGB_8888;
            FileChannel channel = randomAccessFile.getChannel();
            MappedByteBuffer map = channel.map(FileChannel.MapMode.READ_WRITE, 0, imgIn.getRowBytes()*height);
            imgIn.copyPixelsToBuffer(map);
            //recycle the source bitmap, this will be no longer used.
            imgIn.recycle();
            System.gc();// try to force the bytes from the imgIn to be released

            //Create a new bitmap to load the bitmap again. Probably the memory will be available.
            imgIn = Bitmap.createBitmap(width, height, type);
            map.position(0);
            //load it back from temporary
            imgIn.copyPixelsFromBuffer(map);
            //close the temporary file and channel , then delete that also
            channel.close();
            randomAccessFile.close();

            // delete the temp file
            file.delete();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return imgIn;
    }

    // this method changes the color transparency by the factor
    public static int adjustAlpha(int color, float factor) {
        int alpha = Math.round(Color.alpha(color) * factor);
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        return Color.argb(alpha, red, green, blue);
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;
        if (drawable != null) {
            if (drawable instanceof BitmapDrawable) {
                return ((BitmapDrawable) drawable).getBitmap();
            }

            // We ask for the bounds if they have been set as they would be most
            // correct, then we check we are  > 0
            final int width = !drawable.getBounds().isEmpty() ?
                    drawable.getBounds().width() : drawable.getIntrinsicWidth();

            final int height = !drawable.getBounds().isEmpty() ?
                    drawable.getBounds().height() : drawable.getIntrinsicHeight();

            // Now we check we are > 0
            bitmap = Bitmap.createBitmap(width <= 0 ? 1 : width, height <= 0 ? 1 : height,
                    Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
        }
        return bitmap;
    }


}
