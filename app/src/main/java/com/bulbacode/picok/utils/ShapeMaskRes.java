package com.bulbacode.picok.utils;

import android.content.res.Resources;

import com.bulbacode.picok.R;
import com.bulbacode.picok.WorkView;
import com.bulbacode.picok.activities.EditActivity;

/**
 * Created by Hadadi on 1/6/2016.
 *
 * Manages the current shape mask by iys properties
 */
public class ShapeMaskRes {
    private static final String DEFAULT_SHAPE = "cir1_cir1";

    private static boolean isMultiplied = false;

    private static Resources resources = null;
    private static final int disabledShape = R.raw.fullsquare;

    private static String name;
    private static boolean isPreview;
    private static int svg;

    public ShapeMaskRes() {
        resources = EditActivity.getActivity().getResources();
        isPreview = true;

        // load the default preview circle shape
        loadShape(DEFAULT_SHAPE);
    }

    public static int loadShape(String shapeName) {
        name = shapeName;

        svg = findShape(isMultiplied);

        return svg;
    }

    public static int switchMultiplied() {
        return setIsMultiplied(!isMultiplied);
    }

    public static int setIsMultiplied(boolean newIsMultiplied) {
        if (isMultiplied == newIsMultiplied)
            return svg;

        isMultiplied = newIsMultiplied;
        if (WorkView.getInstance().getMaskSize() == 3)
            svg = getNotMultipliedVersion();
        else
            svg = findShape(isMultiplied);

        return svg;
    }

    public static int getNotMultipliedVersion() {
        svg = findShape(false);
        return svg;
    }

    public static int getMultipliedVersion() {
        svg = findShape(true);
        return svg;
    }

    public static int findShape(boolean _isMultiplied) {
        String fullName = getFullName(_isMultiplied);

        // Getting the resource drawable by calculating it's full name
        return resources.getIdentifier("raw/" + fullName, null,
                EditActivity.getActivity().getPackageName());
    }

    public void setIsPreview(boolean newIsPreview) {
        if (isPreview == newIsPreview)
            return;

        isPreview = !isPreview;
    }

    private static String getFullName(boolean _isMultiplied) {
        String fullname = name;

        if (_isMultiplied)
            fullname += "_mul";

        return fullname;
    }

    public static boolean getIsMultiplied() {
        return isMultiplied;
    }

    public static int getShape() {
        return svg;
    }

    public static boolean getIsPreview() {
        return isPreview;
    }

    public static int getDisabledShape() {
        return disabledShape;
    }
}
