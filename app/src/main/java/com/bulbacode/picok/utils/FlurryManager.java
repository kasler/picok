package com.bulbacode.picok.utils;

import com.bulbacode.picok.WorkView;
import com.bulbacode.picok.worktools.ShapeTool;
import com.flurry.android.FlurryAgent;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sergey on 4/16/16.
 */
public class FlurryManager {

    private static final String USERNAME = "Username";
    private static final String DESIGN_SESSION = "Start_Design_Session";
    private static final String SHARE_METHOD = "Share_Method";
    private static final String BASE_SHAPE_EVENT = "Base_Shape";
    private static final String CHILD_SHAPE_EVENT = "Child_Shape";
    private static final String START_APPLICATION_EVENT = "Start_Application_Event";
    private static final String BASE_SHAPE_EVENT_NEW = "BASE_SHAPE";
    private static final String CHILD_SHAPE_EVENT_NEW = "CHILD_SHAPE";
    private static final String FEATURE = "FEATURE";

    private String username;

    private Map<String, String> designSessionParams = new HashMap<String, String>();
    private Map<String, String> baseShapeParams = new HashMap<String, String>();
    private Map<String, String> childShapeParams = new HashMap<String, String>();
    private Map<String, String> featureParams = new HashMap<String, String>();
    private Map<String, String> shareParams = new HashMap<String, String>();
    private boolean isSessionStarted;

    private static FlurryManager instance;


    public static FlurryManager getInstance() {
        if (instance == null) {
            instance = new FlurryManager();
        }
        return instance;
    }


//    public void startApplicationEvent() {
//        FlurryAgent.logEvent(START_APPLICATION_EVENT, true);
//    }

    public void startDesignSession() {
        FlurryAgent.logEvent(DESIGN_SESSION, true);
        isSessionStarted = true;

    }

    public void baseShapeEvent(ShapeTool.ShapeFamilyCode baseShape) {
        baseShapeParams.put(BASE_SHAPE_EVENT, baseShape.name());
        FlurryAgent.logEvent(BASE_SHAPE_EVENT, baseShapeParams);
        baseShapeParams.clear();
    }

    public void childShapeEvent(String childShape) {
        childShapeParams.put(CHILD_SHAPE_EVENT, childShape);
        FlurryAgent.logEvent(CHILD_SHAPE_EVENT, childShapeParams);
        childShapeParams.clear();
    }

    public void featureEvent(WorkView.ToolFeatureEnum feature) {
        featureParams.put(FEATURE, feature.name());
        FlurryAgent.logEvent(FEATURE, featureParams);
        featureParams.clear();
    }

    // TODO seperate this into two methods ? save and share ?
    public void shareEvent(String shareMethod) {
        shareParams.put(SHARE_METHOD, shareMethod);
        FlurryAgent.logEvent(SHARE_METHOD, shareParams);
        shareParams.clear();
    }

    public void endSession() {
        if (isSessionStarted) {
            FlurryAgent.endTimedEvent(DESIGN_SESSION);
            isSessionStarted = !isSessionStarted;
        }
    }


    private FlurryManager() {

    }
}
