package com.bulbacode.picok.utils;

import android.content.Context;
import android.util.AttributeSet;

import com.bulbacode.picok.R;
import com.bulbacode.picok.activities.EditActivity;
import com.github.siyamed.shapeimageview.ShaderImageView;
import com.github.siyamed.shapeimageview.shader.ShaderHelper;
import com.github.siyamed.shapeimageview.shader.SvgShader;

/**
 * ye-ye
 *
 * Created by Hadadi on 2/9/2016.
 */
public class PorterSVGImageView extends ShaderImageView {

    public PorterSVGImageView(Context context) {
        super(context);
    }

    public PorterSVGImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PorterSVGImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public ShaderHelper createImageViewHelper() {
        return new SvgShader(R.raw.cir1_cir1);
    }

    public void setSize(int newSize) {
        getPathHelper().setSize(newSize);
        redraw();
    }

    public int getSize() {
        return getPathHelper().getSize();
    }

    public void redraw() {
        getPathHelper().redraw();
        invalidate();
    }

    public void setShape(int resID) {
        setShape(resID, getPathHelper().getShapeRotation());
    }

    public void setShape(int resID, float rotation) {
        ((SvgShader) getPathHelper()).setShapeResId(EditActivity.getActivity(), resID);

        setShapeRotation(rotation);
    }

    public void setShapeRotation(float rotation) {
        getPathHelper().setShapeRotation(rotation);
        redraw();
    }

    public float getShapeRotation() {
        return getPathHelper().getShapeRotation();
    }

    public void setZoom(int newZoom, boolean justTrackZoomLevel) {
        getPathHelper().setZoom(newZoom);

        if (!justTrackZoomLevel)
            redraw();
    }

    public int getZoom() {
        return getPathHelper().getZoom();
    }

    public void disableMask() {
        setShape(ShapeMaskRes.getDisabledShape());
        getPathHelper().setIsMaskEnabled(false);
    }

    public void enableMask() {
        getPathHelper().setIsMaskEnabled(true);
        setShape(ShapeMaskRes.getShape());
    }
}