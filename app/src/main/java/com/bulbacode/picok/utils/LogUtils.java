package com.bulbacode.picok.utils;

import android.util.Log;

/**
 * Created by sergey on 1/21/16.
 */
public class LogUtils {

    public static void printString (String TAG, String var, String varName) {
        Log.d(TAG, varName + ": " + var);
    }

}
