package com.bulbacode.picok.test;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.ViewGroup;

/**
 * Created by sergey on 1/26/16.
 */
public class SimpleSeekBarViewGroup extends ViewGroup {

    private SeekBarThumbable mSeekBar;
    private final static String ANDROIDNS = "http://schemas.android.com/apk/res/android";
    private final static String ANDROIDTOOLS = "http://schemas.android.com/tools";
    private TextPaint mTextPaint;
    private int mTextSize;
    private int mTextColor;
    private Typeface mTextStyle;

    public SimpleSeekBarViewGroup(Context context) {
        super(context);
        mSeekBar = new SeekBarThumbable(context);
        init(null);
    }



    public SimpleSeekBarViewGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
        mSeekBar = new SeekBarThumbable(context, attrs);
        init(attrs);
    }

    public SimpleSeekBarViewGroup(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mSeekBar = new SeekBarThumbable(context, attrs, defStyleAttr);
        init(attrs);
    }



    /**
     * Initializes Seek bar extended attributes from xml
     *
     * @param attrs {@link AttributeSet}
     */

    private void init(AttributeSet attrs) {
        // init the xml attributes (seekbar: thumb, progress)
        if (attrs != null) {
//            int thumbResource = attrs.getAttributeResourceValue(android.R.attr.thumb, 0);
//            int progressResource = attrs.getAttributeIntValue(android.R.attr.progress, 0);
//            int thumbResource = attrs.getAttributeResourceValue(ANDROIDNS, "thumb", 0);
//            int progressResource = attrs.getAttributeResourceValue(ANDROIDTOOLS, "progressDrawable", 0);
//
//            Drawable thumbDrawable = getResources().getDrawable(thumbResource);
//            Drawable progressDrawable = getResources().getDrawable(progressResource);
//
//            mSeekBar.setThumb(thumbDrawable);
//            mSeekBar.setProgressDrawable(progressDrawable);


        }

        // Now get needed Text attributes
        final int textSizeResource = attrs.getAttributeResourceValue(ANDROIDNS, "textSize", 0);

        if (0 != textSizeResource) {
            mTextSize = getResources().getDimensionPixelSize(textSizeResource);
        }

        final int textColorResource = attrs.getAttributeResourceValue(ANDROIDNS, "textColor", 0);

        if (0 != textColorResource) {
            mTextColor = getResources().getColor(textColorResource);
        }

        final int typeface = attrs.getAttributeIntValue(ANDROIDNS, "textStyle", 0);

        switch (typeface) {
            // normale
            case 0:
                mTextStyle = Typeface.create(Typeface.DEFAULT, Typeface.NORMAL);
                break;
            // bold
            case 1:
                mTextStyle = Typeface.create(Typeface.DEFAULT, Typeface.BOLD);
                break;
            // italic
            case 2:
                mTextStyle = Typeface.create(Typeface.DEFAULT, Typeface.ITALIC);
                break;
            // bold | italic
            case 3:
                mTextStyle = Typeface.create(Typeface.DEFAULT, Typeface.BOLD_ITALIC);
                break;
        }

        mTextPaint = new TextPaint();
        mTextPaint.setColor(mTextColor);
        mTextPaint.setTextSize(mTextSize);
        mTextPaint.setTypeface(mTextStyle);
        mTextPaint.setTextAlign(Paint.Align.LEFT);
        mTextPaint.setStyle(Paint.Style.FILL);


        // TODO should be dynamic ? thumb offset works
        mSeekBar.setThumbOffset(22);

//        mSeekBar.offsetLeftAndRight(50);
        addView(mSeekBar);


        // init txt paint
        // TODO add a rectangle

    }

    // onMeasure()

    /**
     * onLayout() should be called on each view inside the viewGroup
     * left and top are normally zero
     *
     * @param l,t,r,b{@link AttributeSet}
     */
    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        // use a rect from init
        int bottom = mSeekBar.getSeekBarThumb().getBounds().bottom;
        int right = r - 100;
        int left = 50;

        mSeekBar.layout(left, 0, right, bottom);
    }

    /**
     * onDraw() is called after onMeasure(), and onLayout() in order to draw each view
     *
     * @param canvas {@link AttributeSet}
     */

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
//
//        mSeekBar.setProgressDrawable(getResources().getDrawable( R.drawable.styled_progress));
//        // draw seekBar
//        // draw the number above the progress
        mSeekBar.draw(canvas);
    }




}
