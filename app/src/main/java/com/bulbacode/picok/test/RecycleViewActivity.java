package com.bulbacode.picok.test;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.bulbacode.picok.RecyclerViewAdapter;

public class RecycleViewActivity extends Activity implements RecyclerViewAdapter.OnItemClickListener {

    private RecyclerView myRecyclerView;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerViewAdapter myRecyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

        myRecyclerView = new RecyclerView(this);

        linearLayoutManager =
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        /*
        linearLayoutManager =
                new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        */

        myRecyclerViewAdapter = new RecyclerViewAdapter(this);
        myRecyclerViewAdapter.setOnItemClickListener(this);
        myRecyclerView.setAdapter(myRecyclerViewAdapter);
        myRecyclerView.setLayoutManager(linearLayoutManager);

        setContentView(myRecyclerView);

        //insert dummy items
        myRecyclerViewAdapter.add(0, "Eric");
        myRecyclerViewAdapter.add(1, "Android");
        myRecyclerViewAdapter.add(0, "Android-er");
        myRecyclerViewAdapter.add(2, "Google");
        myRecyclerViewAdapter.add(3, "android dev");
        myRecyclerViewAdapter.add(0, "android-er.blogspot.com");
        myRecyclerViewAdapter.add(4, "Peter");
        myRecyclerViewAdapter.add(4, "Paul");
        myRecyclerViewAdapter.add(4, "Mary");
        myRecyclerViewAdapter.add(4, "May");
        myRecyclerViewAdapter.add(4, "Divid");
        myRecyclerViewAdapter.add(4, "Frankie");
    }

    @Override
    public void onItemClick(RecyclerViewAdapter.ItemHolder item, int position) {
        Toast.makeText(this,
                position + " : " + item.getItemName(),
                Toast.LENGTH_SHORT).show();
    }


}
