package com.bulbacode.picok.test;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.widget.SeekBar;

import com.bulbacode.picok.utils.PixelUtils;
import com.bulbacode.picok.R;

/**
 * Created by sergey on 1/27/16.
 */
public class CustomSeekBar extends SeekBar {

    // TODO check if the Y_OFFSET is in DP ?
    public static final int Y_OFFSET = 5;
    // TODO check if the size is in SP
    public static final int TEXT_SIZE = 25;
    private static final int X_OFFSET = 16;

    private TextPaint mTextPaint;

    private Drawable mThumb;

    public CustomSeekBar(Context context) {
        super(context);
    }

    public CustomSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CustomSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }



    @Override
    public void setThumb(Drawable thumb) {
        super.setThumb(thumb);
        mThumb = thumb;
    }

    /**
     * This method inits SeekBar thumb and progression method using xml attributes or
     * if null will set the default one.
     *
     * @param attrs {@link AttributeSet}
     */
    // TODO add height xml parameter of the progress text label ?

    public void init(AttributeSet attrs) {
        mTextPaint = new TextPaint();
        mTextPaint.setColor(ContextCompat.getColor(this.getContext(), R.color.picok_pink));
        mTextPaint.setTextSize(TEXT_SIZE);
//        mTextPaint.setTypeface();
        mTextPaint.setTextAlign(Paint.Align.LEFT);
        mTextPaint.setStyle(Paint.Style.FILL);

        setProgress(0);
    }

    public Drawable getSeekBarThumb() {
        return mThumb;
    }


//    @Override
//    protected synchronized void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//
//        int width = super.getMeasuredWidth();
//        int height = super.getMeasuredHeight();
//
//        setMeasuredDimension(width, height + Y_OFFSET);
//    }

    /**
     * This method draws our view, extending the basics. super.onDraw()
     * will draw the SeekBar and we add our text edition above the X of the
     * thumb.
     *
     * @param canvas {@link AttributeSet}
     */

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        // seekbar
        super.onDraw(canvas);

        // progress string
        int progress = this.getProgress();
        String progressString = String.valueOf(progress);

        // x position of the thumb
        Rect rect = mThumb.getBounds();
        int thumbX = rect.centerX();
        int positionX = thumbX;

        // check the number of digits to decide the offset
        float pixels;
        int dpi;

        if (progress > 99) { // three digits
            dpi = 10;
//            positionX -= X_OFFSET * 2;

        } // two digits
        else if (progress > 9) {
            dpi = 6;
//            positionX -= X_OFFSET * 1.5;
        }
        else {  // one digit
            dpi = 3;
//            positionX -= X_OFFSET;
        }

        pixels = PixelUtils.convertDpToPixel(dpi, getContext());
        positionX -= pixels;

        // the height of the whole thing
        int offestY = this.getThumbOffset();

//      TODO check if progress 0 then color it grey, else red
//      TODO check if progress is a digit, a number or a hundred and decide the thumbX by that
//        canvas.drawText(progress, thumbX - offset/2, thumbY + Y_OFFSET, mTextPaint);
        canvas.drawText(progressString, positionX, offestY + Y_OFFSET, mTextPaint);
    }
}
