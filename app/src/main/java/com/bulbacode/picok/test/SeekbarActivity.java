package com.bulbacode.picok.test;

import android.app.Activity;
import android.os.Bundle;
import android.widget.SeekBar;

import com.bulbacode.picok.R;

public class SeekbarActivity extends Activity {

    private SeekBar mybar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seekbar);
        mybar = (SeekBar) findViewById(R.id.seekBar1);

        mybar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                //add here your implementation
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

                //add here your implementation
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {

                //add here your implementation
            }
        });
    }

}
