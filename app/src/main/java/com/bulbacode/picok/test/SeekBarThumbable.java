package com.bulbacode.picok.test;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.SeekBar;

/**
 * Created by sergey on 1/26/16.
 */
public class SeekBarThumbable extends SeekBar{
    public SeekBarThumbable(Context context) {
        super(context);
    }
    public SeekBarThumbable(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public SeekBarThumbable(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    Drawable mThumb;
    @Override
    public void setThumb(Drawable thumb) {
        super.setThumb(thumb);
        mThumb = thumb;
    }
    public Drawable getSeekBarThumb() {
        return mThumb;
    }
}
