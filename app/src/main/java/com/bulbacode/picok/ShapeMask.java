package com.bulbacode.picok;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import com.bulbacode.picok.activities.EditActivity;

/**
 * Created by Hadadi on 1/6/2016.
 *
 * Manages the current shape mask by iys properties
 */
public class ShapeMask {
    private static final String DEFAULT_SHAPE = "circle1";

    private static boolean isMultiplied = false;

    private final Resources resources;
    private final BitmapFactory.Options options;

    private String name;
    private boolean isPreview;
    private BitmapDrawable bitmap;
    private boolean isFirstLoad;

    public ShapeMask() {
        this.resources = EditActivity.getActivity().getResources();

        this.isFirstLoad = true;
        this.isPreview = true;

        // set the size to option, the images we will load by using this option
        options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inMutable = true;
        BitmapFactory.decodeResource(resources, R.drawable.circle1_p, options);

        // we will create empty bitmap by using the option
        bitmap = new BitmapDrawable(resources,
                            Bitmap.createBitmap(options.outWidth, options.outHeight, Bitmap.Config.ARGB_8888));

        // set the option to allocate memory for the bitmap
        options.inJustDecodeBounds = false;
        options.inSampleSize = 1;
        //options.inBitmap = bitmap;
        options.inMutable = true;

        // load the default preview circle shape
        loadShape(DEFAULT_SHAPE);
    }

    public Drawable loadShape(String shapeName) {
        this.name = shapeName;

        this.bitmap.getBitmap().recycle();
        this.bitmap = findShape();

        if (this.isFirstLoad)
            this.isFirstLoad = false;

        return this.bitmap;
    }

    public Drawable switchMultiplied() {
        return setIsMultiplied(!this.isMultiplied);
    }

    public Drawable setIsMultiplied(boolean isMultiplied) {
        if (this.isMultiplied == isMultiplied)
            return this.bitmap;

        BitmapDrawable newBitmap = findShape();

        if (newBitmap == null) {
            return null;
        }
        else {
            this.isMultiplied = isMultiplied;
            this.bitmap.getBitmap().recycle();
            this.bitmap = newBitmap;

            return this.bitmap;
        }
    }

    private BitmapDrawable findShape() {
        String fullName = getFullName();

        // Getting the resource drawable by calculating it's full name
        final int resourceId = resources.getIdentifier(fullName, "drawable",
                EditActivity.getActivity().getPackageName());

        if (resourceId != 0) {
            return new BitmapDrawable(resources, BitmapFactory.decodeResource(resources, resourceId));
        }
        else {
            // TODO ERROR
            return null;
        }
    }

    public Drawable setIsPreview(boolean isPreview) {
        if (this.isPreview == isPreview)
            return this.bitmap;

        BitmapDrawable newDrawable = findShape();

        if (newDrawable != null) {
            this.isPreview = isPreview;
            this.bitmap.getBitmap().recycle();
            this.bitmap = newDrawable;
            return this.bitmap;
        }
        else {
            return null;
        }
    }

    private String getFullName() {
        String fullname = name;

        if (isMultiplied)
            fullname += "_mul";

        if (isPreview)
            fullname += "_p";

        return fullname;
    }

    public static boolean getIsMultiplied() {
        return isMultiplied;
    }

    public Drawable getShape() {
        return this.bitmap;
    }

    public boolean getIsPreview() {
        return isPreview;
    }
}
