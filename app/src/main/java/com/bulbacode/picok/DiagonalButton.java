package com.bulbacode.picok;

/**
 * Created by sergey on 6/21/16.
 */

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Path.FillType;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.Toast;

public class DiagonalButton extends Button {

    public static final float DEF_GAP = 1.0f;
    private Path path;
    private static int gap;
    private Paint paint;

    private boolean isSingle = true;
    private boolean isTextureMode = false;
    private int singleColor;
    private int upperColor;
    private int lowerColor;
    private int gapColor;
    private float gapDimension;

    private Bitmap singleBitmap;
    private Bitmap upperBitmap;
    private Bitmap lowerBitmap;

    private BitmapShader singleShader;
    private BitmapShader upperShader;
    private BitmapShader lowerShader;
    private int height;
    private int width;

    public DiagonalButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public void setIsSingleColor(boolean isSingleColor) {
        this.isSingle = isSingleColor;
        invalidate();
    }

    public void setSingle(int color) {
        this.isSingle = true;
        this.singleColor = color;
        invalidate();
    }

    public void setUpperColor(int color) {
        this.isSingle = false;
        this.upperColor = color;
        invalidate();
    }

    public void setLowerColor(int color) {
        this.isSingle = false;
        this.lowerColor = color;
        invalidate();
    }

    public void setGapDimension(float gapDimension) {
        this.gapDimension = gapDimension;
        this.gap = (int) gapDimension;
        invalidate();
    }

    public void setSingleTextureBitmap(Bitmap bitmap) {
        isSingle = true;
        singleBitmap = bitmap;
        singleShader = new BitmapShader(singleBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        invalidate();
    }

    public void setUpperTextureBitmap(Bitmap bitmap) {
        isSingle = false;
        upperBitmap = bitmap;
        upperShader = new BitmapShader(upperBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        invalidate();
    }

    public void setLowerTextureBitmap(Bitmap bitmap) {
        isSingle = false;
        lowerBitmap = bitmap;
        lowerShader = new BitmapShader(lowerBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        invalidate();
    }

    private void init(AttributeSet attrs) {
        paint = new Paint();

        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.DiagonalButtonColors);
        final int size = ta.getIndexCount();
        // setting all custom attributes
        for (int i = 0 ; i < size; i++) {
            int attrIndex = ta.getIndex(i);
            switch (attrIndex) {
                case R.styleable.DiagonalButtonColors_is_single_color:
                    isSingle = ta.getBoolean(attrIndex, true);
                    break;
                case R.styleable.DiagonalButtonColors_single_color:
                    singleColor = ta.getColor(attrIndex, Color.WHITE);
                    break;
                case R.styleable.DiagonalButtonColors_upper_color:
                    upperColor = ta.getColor(attrIndex, Color.BLUE);
                    break;
                case R.styleable.DiagonalButtonColors_lower_color:
                    lowerColor = ta.getColor(attrIndex, Color.RED);
                    break;
                case R.styleable.DiagonalButtonColors_gap_color:
                    gapColor = ta.getColor(attrIndex, Color.GRAY);
                    break;
                case R.styleable.DiagonalButtonColors_gap_dimension:
                    gapDimension = ta.getDimension(attrIndex, DEF_GAP);
                    gap = (int) gapDimension;
                    break;
                case R.styleable.DiagonalButtonColors_is_texture_mode:
                    isTextureMode = ta.getBoolean(attrIndex, false);
                    break;
                case R.styleable.DiagonalButtonColors_single_texture:
                    Drawable drawable = ta.getDrawable(attrIndex);
                    singleBitmap = decodeBitmap(drawable);
                    singleShader = new BitmapShader(singleBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);

                    break;
                case R.styleable.DiagonalButtonColors_upper_texture:
                    drawable = ta.getDrawable(attrIndex);
                    upperBitmap = decodeBitmap(drawable);
                    upperShader = new BitmapShader(upperBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
                    break;
                case R.styleable.DiagonalButtonColors_lower_texture:
                    drawable = ta.getDrawable(attrIndex);
                    lowerBitmap = decodeBitmap(drawable);
                    lowerShader = new BitmapShader(lowerBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
                    break;
            }
        }

        ta.recycle();
    }

    private Bitmap decodeBitmap(Drawable drawable) {
        Bitmap bitmap = null;
        try {
            bitmap = ((BitmapDrawable) drawable).getBitmap();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getContext(), "Can't decode bitmap", Toast.LENGTH_SHORT).show();
        }
        return bitmap;
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawColor(gapColor);
        paint.setStrokeWidth(0);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setAntiAlias(true);

        height = getHeight();
        width = getWidth();

        if (isSingle) {
            drawSingleColor(canvas);

        }
        else { // multi color
            drawMultiColor(canvas);

        }
    }

    private void drawMultiColor(Canvas canvas) {
        // upper triangle
        if (isTextureMode) {
            paint.setShader(upperShader);
        }

        paint.setColor(upperColor);

        Point a = new Point(gap, gap);
        Point b = new Point(gap, height - 2 * gap);
        Point c = new Point(width - 2 * gap, gap);

        drawTriangle(canvas, a, b, c);

        // lower triangle
        if (isTextureMode) {
            paint.setShader(lowerShader);
        }
        paint.setColor(lowerColor);

        Point a1 = new Point(width - gap, height - gap);
        Point b1 = new Point(width - gap, 2 * gap);
        Point c1 = new Point(2 * gap, height - gap);

        drawTriangle(canvas, a1, b1, c1);
    }

    private void drawTriangle(Canvas canvas, Point a, Point b, Point c) {


        path = new Path();
        path.setFillType(FillType.EVEN_ODD);
        path.moveTo(a.x, a.y);
        path.lineTo(b.x, b.y);
        path.lineTo(c.x, c.y);
        path.close();

        canvas.drawPath(path, paint);

    }

    private void drawSingleColor(Canvas canvas) {
        if (isTextureMode) {
            singleColor = Color.WHITE;
            paint.setShader(singleShader);
        }
        else {
            paint.setColor(singleColor);
        }

        Rect rect = new Rect(gap, gap, width - gap, height - gap);
        canvas.drawRect(rect, paint);
    }
}