package com.bulbacode.picok;

import android.content.res.Resources;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.bulbacode.picok.activities.EditActivity;
import com.bulbacode.picok.worktools.ShapeTool;

import java.util.ArrayList;

/**
 * Created by Hadadi on 1/20/2016.
 */
public class ShapeList {
    private final ShapeListAdapter adapter;
    private final ArrayList<ShapeChoice[]> shapes;

    public ShapeList(RecyclerView recyclerView) {
        Resources resources = EditActivity.getActivity().getResources();

        // this is data fro recycler view
        this.shapes = new ArrayList<>(4);

        ShapeChoice[] circles = new ShapeChoice[]{
                new ShapeChoice("cir1", true, resources),
                new ShapeChoice("cir2", true, resources),
                new ShapeChoice("cir3", true, resources),
                new ShapeChoice("cir4", true, resources),
                new ShapeChoice("cir5", true, resources),
                new ShapeChoice("cir6", true, resources),
                new ShapeChoice("cir7", true, resources),
                new ShapeChoice("cir8", true, resources),
                new ShapeChoice("cir9", true, resources),
                new ShapeChoice("cir10", true, resources),
                new ShapeChoice("cir11", true, resources),
                new ShapeChoice("cir12", true, resources),
                new ShapeChoice("cir13", true, resources),
                new ShapeChoice("cir14", true, resources),
                new ShapeChoice("cir15", true, resources),
                new ShapeChoice("cir16", true, resources),
                new ShapeChoice("cir17", true, resources),
                new ShapeChoice("cir18", true, resources),
                new ShapeChoice("cir19", true, resources),
                new ShapeChoice("cir20", true, resources),
                new ShapeChoice("cir21", true, resources),
                new ShapeChoice("cir22", true, resources),
                new ShapeChoice("cir23", true, resources)};

        ShapeChoice[] squares = new ShapeChoice[]{ new ShapeChoice("rec1", true, resources),
                new ShapeChoice("rec2", true, resources),
                new ShapeChoice("rec3", true, resources),
                new ShapeChoice("rec4", true, resources),
                new ShapeChoice("rec5", true, resources),
                new ShapeChoice("rec6", true, resources),
                new ShapeChoice("rec7", true, resources),
                new ShapeChoice("rec8", true, resources),
                new ShapeChoice("rec9", true, resources),
                new ShapeChoice("rec10", true, resources),
                new ShapeChoice("rec11", true, resources),
                new ShapeChoice("rec12", true, resources),
                new ShapeChoice("rec13", true, resources),
                new ShapeChoice("rec14", true, resources),
                new ShapeChoice("rec15", true, resources),
                new ShapeChoice("rec16", true, resources),
                new ShapeChoice("rec17", true, resources),
                new ShapeChoice("rec18", true, resources),
                new ShapeChoice("rec19", true, resources),
                new ShapeChoice("rec20", true, resources),
                new ShapeChoice("rec21", true, resources),
                new ShapeChoice("rec22", true, resources),
                new ShapeChoice("rec23", true, resources)};

        ShapeChoice[] stars = new ShapeChoice[]{ new ShapeChoice("star1", true, resources),
                new ShapeChoice("star2", true, resources),
                new ShapeChoice("star3", true, resources),
                new ShapeChoice("star4", true, resources),
                new ShapeChoice("star5", true, resources),
                new ShapeChoice("star6", true, resources),
                new ShapeChoice("star7", true, resources),
                new ShapeChoice("star8", true, resources),
                new ShapeChoice("star9", true, resources),
                new ShapeChoice("star10", true, resources),
                new ShapeChoice("star11", true, resources),
                new ShapeChoice("star12", true, resources),
                new ShapeChoice("star13", true, resources),
                new ShapeChoice("star14", true, resources),
                new ShapeChoice("star15", true, resources),
                new ShapeChoice("star16", true, resources),
                new ShapeChoice("star17", true, resources),
                new ShapeChoice("star18", true, resources),
                new ShapeChoice("star19", true, resources),
                new ShapeChoice("star20", true, resources),
                new ShapeChoice("star21", true, resources),
                new ShapeChoice("star22", true, resources),
                new ShapeChoice("star23", true, resources),
                new ShapeChoice("star24", true, resources),
                new ShapeChoice("star25", true, resources),
                new ShapeChoice("star26", true, resources),
                new ShapeChoice("star27", true, resources),
                new ShapeChoice("star28", true, resources),
                new ShapeChoice("star29", true, resources),
                new ShapeChoice("star30", true, resources),
                new ShapeChoice("star31", true, resources)};

        ShapeChoice[] triangles = new ShapeChoice[]{ new ShapeChoice("tri1", true, resources),
                new ShapeChoice("tri2", true, resources),
                new ShapeChoice("tri3", true, resources),
                new ShapeChoice("tri4", true, resources),
                new ShapeChoice("tri5", true, resources),
                new ShapeChoice("tri6", true, resources),
                new ShapeChoice("tri7", true, resources),
                new ShapeChoice("tri8", true, resources),
                new ShapeChoice("tri9", true, resources),
                new ShapeChoice("tri10", true, resources),
                new ShapeChoice("tri11", true, resources),
                new ShapeChoice("tri12", true, resources),
                new ShapeChoice("tri13", true, resources),
                new ShapeChoice("tri14", true, resources),
                new ShapeChoice("tri15", true, resources),
                new ShapeChoice("tri16", true, resources),
                new ShapeChoice("tri17", true, resources),
                new ShapeChoice("tri18", true, resources),
                new ShapeChoice("tri19", true, resources),
                new ShapeChoice("tri20", true, resources),
                new ShapeChoice("tri21", true, resources),
                new ShapeChoice("tri22", true, resources),
                new ShapeChoice("tri23", true, resources)};

        shapes.add(circles);
        shapes.add(squares);
        shapes.add(triangles);
        shapes.add(stars);

        // Set layoutManger
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(EditActivity.getActivity(), LinearLayoutManager.HORIZONTAL, false);

        recyclerView.setLayoutManager(linearLayoutManager);

        // Create and set the adapter
        this.adapter = new ShapeListAdapter(shapes.get(ShapeTool.ShapeFamilyCode.CIRCLES.value));
        recyclerView.setAdapter(adapter);

        // Set item animator to DefaultAnimator
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    public void loadShapes(ShapeTool.ShapeFamilyCode shapeFamilyCode) {
        if (shapeFamilyCode.value < this.shapes.size())
            this.adapter.loadShapes(this.shapes.get(shapeFamilyCode.value));
    }

    public void refresh() {
        this.adapter.notifyDataSetChanged();
    }
}
