package com.bulbacode.picok;

import android.annotation.TargetApi;
import android.os.Build;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by Hadadi on 1/12/2016.
 *
 * A barrage of buttons for changing a scale of whatever
 */
public abstract class PicokScale implements View.OnClickListener {
    private ImageButton bigButton;
    private ArrayList<ImageButton> buttonsList = null;
    private ArrayList<ImageView> linesList = null;

    protected int curActiveScale = -1;
    protected int defaultScale = -1;

    // TODO Missing stuff
    private final int activeRes = R.drawable.bullet_big_sel;
    private final int inactiveRes = R.drawable.bullet;
    private final int unabledRes = R.drawable.bullet_dis;
    private final int defaultActiveRes = R.drawable.bullet_big_none;
    private final int defaultInactiveRes = R.drawable.bullet_none;
    private final int inactiveLineRes = R.drawable.line_dis;
    private final int activeLineRes = R.drawable.zoom_line;

    public PicokScale(int scaleSize, ImageButton bigButton, final View frame) {
        if (scaleSize < 1) {
            // TODO Error
            return;
        }

        this.buttonsList = new ArrayList<>(scaleSize);
        this.linesList = new ArrayList<>(scaleSize -1);

        this.bigButton = bigButton;
        this.bigButton.setVisibility(View.GONE);

        frame.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    @Override
                    public void onGlobalLayout() {
                        // Layout has happened here.
                        setActiveIndex(curActiveScale);

                        // Don't forget to remove your listener when you are done with it.
                        if (Build.VERSION.SDK_INT < 16) {
                            frame.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        } else {
                            frame.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        }
                    }
                });
    }

    public void add(ImageButton button, ImageView followingLine, boolean isEnabled) {
        this.add(button, followingLine, isEnabled, false);
    }

    public void addDefault(ImageButton button, ImageView followingLine, boolean isEnabled) {
        this.add(button, followingLine, isEnabled, true);
    }

    private void add(ImageButton button, ImageView followingLine, boolean isEnabled, boolean isDefault) {
        if (this.buttonsList == null ||
                button == null)
            return;

        this.buttonsList.add(button);

        if (followingLine != null)
            this.linesList.add(followingLine);

        int scaleIndex = this.buttonsList.size() - 1;

        if (isDefault) {
            setAsDefault(scaleIndex);
            setActiveIndex(scaleIndex);
        }
        else {
            setIsEnabled(scaleIndex, isEnabled);
        }
    }

    private void setAsDefault(int scaleIndex) {
        this.defaultScale = scaleIndex;
    }

    @Override
    public void onClick(View v) {
        ImageButton button = (ImageButton) v;
        int newScale = this.buttonsList.indexOf(button);

        // If not already pressed
        if (this.curActiveScale != newScale)
            onScaled(this.curActiveScale, newScale);

        setActiveIndex(newScale);
    }

    public void setActiveIndex(int scaleIndex) {
        if (scaleIndex < 0 || scaleIndex > this.buttonsList.size())
            // TODO Error
            return;

        // deactivating previous active button
        if (this.curActiveScale > -1) {
            ImageButton button = this.buttonsList.get(this.curActiveScale);
            if (this.curActiveScale == defaultScale) {
                button.setImageResource(defaultInactiveRes);
            }
            else {
                button.setImageResource(inactiveRes);
            }
        }

        this.curActiveScale = scaleIndex;

        ImageButton button = this.buttonsList.get(scaleIndex);
        if (scaleIndex == defaultScale) {
            // TODO BIG button.setImageResource(defaultActiveRes);
            bigButton.setImageResource(defaultActiveRes);
        }
        else {
            // TODO BIG button.setImageResource(activeRes);
            bigButton.setImageResource(activeRes);
        }

        bigButton.setVisibility(View.VISIBLE);
        bigButton.setX(button.getX());

        button.setOnClickListener(this);
    }

    public void setIsEnabled(int scaleIndex, boolean isEnabled) {
        if (this.buttonsList == null)
            return;

        if (scaleIndex > this.buttonsList.size()) {
            // TODO Error
            return;
        }

        ImageButton button = this.buttonsList.get(scaleIndex);

        if (isEnabled) {
            if (this.curActiveScale == scaleIndex) {
                //setActiveIndex(scaleIndex);
            }
            else
                if (scaleIndex == this.defaultScale)
                    button.setImageResource(defaultInactiveRes);
                else
                    button.setImageResource(inactiveRes);

            button.setOnClickListener(this);
        }
        else {
            button.setImageResource(unabledRes);
            button.setOnClickListener(null);
        }

        // Enabling/Disabling the connecting lines - before the button and after (if they exist)
        int linesRes = (isEnabled ? this.activeLineRes : this.inactiveLineRes);

        if (scaleIndex > 0)
            this.linesList.get(scaleIndex - 1).setImageResource(linesRes);

        if (scaleIndex < this.buttonsList.size() - 1)
            this.linesList.get(scaleIndex).setImageResource(linesRes);
    }

    public void enableAll() {
        for (int i = 0; i < this.buttonsList.size(); i++)
            setIsEnabled(i, true);
    }

    public abstract void onScaled(int oldScale, int newScale);
}