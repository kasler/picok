package com.bulbacode.picok;

import android.graphics.Color;

/**
 * Created by Hadadi on 2/3/2016.
 *
 * YEAH
 */
public class PicokImageSettings {
    int zoomLevel;
    int blurLevel;
    boolean isMirrored;
    boolean isFlipped;
    int color;

    // TODO add an alpha factor ?

    public PicokImageSettings(PicokImage obj) {
        if (obj == null) {
            this.zoomLevel = 2;
            this.blurLevel = 0;
            this.isMirrored = false;
            this.isFlipped = false;
            this.color = Color.TRANSPARENT;
        }
        else {
            this.zoomLevel = obj.getZoom();
            this.blurLevel = obj.getBlurLevel();
            this.isMirrored = obj.getIsMirrored();
            this.isFlipped = obj.getIsFlipped();
            this.color = obj.getColor();
        }
    }

    public boolean equals(Object o) {
        if (!(o instanceof PicokImageSettings))
            return false;

        PicokImageSettings other = (PicokImageSettings) o;

        //noinspection RedundantIfStatement
        return (this.zoomLevel == other.zoomLevel
                && this.isFlipped == other.isFlipped
                && this.isMirrored == other.isMirrored
                && this.blurLevel == other.blurLevel);
    }

    public static PicokImageSettings defaultSettings() {
        return new PicokImageSettings(null);
    }
}
