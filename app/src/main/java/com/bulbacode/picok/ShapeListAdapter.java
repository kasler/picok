package com.bulbacode.picok;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.bulbacode.picok.utils.FlurryManager;

import pl.droidsonroids.gif.GifImageButton;

/**
 * Created by Hadadi on 1/20/2016.
 */
public class ShapeListAdapter extends RecyclerView.Adapter<ShapeListAdapter.ViewHolder>{
    private ShapeChoice[] itemsData;

    public ShapeListAdapter(ShapeChoice[] itemsData) {
        loadShapes(itemsData);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ShapeListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                          int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.shape_layout, null);

        // create ViewHolder
        ViewHolder viewHolder = new ViewHolder(itemLayoutView, this);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        ShapeChoice shape = itemsData[position];

        viewHolder.loadViewResources(shape);
    }

    public ShapeChoice getItemAt(int index) {
        if (itemsData.length <= index)
            // TODO ERROR
            return null;

        return itemsData[index];
    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final ShapeListAdapter adapter;
        public final View frame;
        public ImageButton shapeButton;
        public GifImageButton rotateButton;
        public View activeView;

        public ViewHolder(View itemLayoutView, ShapeListAdapter adapter) {
            super(itemLayoutView);

            this.adapter = adapter;

            this.frame = itemLayoutView;

            this.shapeButton = (ImageButton) itemLayoutView.findViewById(R.id.shape_button);
            this.shapeButton.setOnClickListener(this);
            this.shapeButton.setTag("item");

            itemLayoutView.setOnClickListener(this);
            itemLayoutView.setTag("item");

            this.rotateButton = (GifImageButton) itemLayoutView.findViewById(R.id.shape_rotate_button);
            this.rotateButton.setOnClickListener(this);
            this.rotateButton.setTag("rotate");

            this.activeView = itemLayoutView.findViewById(R.id.shape_selected);
        }

        @Override
        public void onClick(View v) {
            if (v.getTag() == null)
                return;

            ShapeChoice shapeChoice = adapter.getItemAt(getAdapterPosition());
            FlurryManager.getInstance().childShapeEvent(shapeChoice.getName());

            // If the shape was clicked or the rotate was clicked when this item isn't selected yet, we select it
            if (v.getTag().equals("item") ||
                    (v.getTag().equals("rotate") && !shapeChoice.getIsSelected())) {
                // It's now active
                shapeChoice.select();

                // Reload all recyclerView's elements again to update their selected/deselected look
                // It will fire all the onBindViewHolder() again
                adapter.notifyDataSetChanged();
            }
            // Else if the rotate button clicked when we are already selected, we rotate the shape
            else if (v.getTag().equals("rotate")) {
                float newRotation = shapeChoice.rotate(this.shapeButton);
                if (newRotation != -1)
                    WorkView.getInstance().rotateMask(newRotation);
            }
        }

        private void loadViewResources(ShapeChoice shape) {
            // Get data from your itemsData at this position
            // Replace the contents of the view with that itemsData
            shape.updateView(this);
        }
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return itemsData.length;
    }

    public void loadShapes(ShapeChoice[] newItems) {
        ShapeChoice.reset(this.itemsData);

        this.itemsData = newItems;
        newItems[0].select();

        notifyDataSetChanged();
    }
}